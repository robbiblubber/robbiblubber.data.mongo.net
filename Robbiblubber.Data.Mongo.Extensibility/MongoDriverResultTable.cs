﻿using System;
using System.Collections.Generic;
using System.Data;



namespace Robbiblubber.Data.Mongo.Extensibility
{
    /// <summary>This class implements a result table.</summary>
    public class MongoDriverResultTable: DataTable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an empty result table.</summary>
        public static readonly MongoDriverResultTable EMPTY = new MongoDriverResultTable();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MongoDriverResultTable(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="recordsAffected">Affected records count.</param>
        public MongoDriverResultTable(int recordsAffected): this()
        {
            RecordsAffected = recordsAffected;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="col">Column name.</param>
        /// <param name="value">Row value.</param>
        /// <param name="recordsAffected">Affected records count.</param>
        public MongoDriverResultTable(string col, object value, int recordsAffected = 0): this(recordsAffected)
        {
            Columns.Add(col, ((value == null) ? typeof(object) : value.GetType()));
            Rows.Add(value);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="prototype">Prototype table.</param>
        /// <param name="contents">New contents.</param>
        public MongoDriverResultTable(MongoDriverResultTable prototype, IEnumerable<DataRow> contents): base()
        {
            foreach(DataColumn i in prototype.Columns) { Columns.Add(i.ColumnName, i.DataType); }
            foreach(DataRow i in contents)
            {
                DataRow r = NewRow();
                foreach(DataColumn k in Columns) { r[k.ColumnName] = i[k.ColumnName]; }
                Rows.Add(r);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets number of records affectd.</summary>
        public int RecordsAffected
        {
            get; set;
        } = 0;
    }
}

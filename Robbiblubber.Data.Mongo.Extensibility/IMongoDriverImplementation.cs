﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Data.Mongo.Extensibility
{
    /// <summary>Driver extensions may implement this interface to faciliate integration.</summary>
    public interface IMongoDriverImplementation
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the executables provided by this implementation.</summary>
        IEnumerable<IMongoDriverExecutable> GetExecutables();
    }
}

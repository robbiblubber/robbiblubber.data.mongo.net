﻿using System;



namespace Robbiblubber.Data.Mongo.Extensibility
{
    /// <summary>MongoDB collection method extensions implement this interface.</summary>
    public interface IMongoDriverCollectionMethod: IMongoDriverMethod, IMongoDriverExecutable
    {}
}

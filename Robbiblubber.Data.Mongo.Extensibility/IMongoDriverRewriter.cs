﻿using System;



namespace Robbiblubber.Data.Mongo.Extensibility
{
    /// <summary>MongoDB driver rewriter extensions implement this interface.</summary>
    public interface IMongoDriverRewriter: IMongoDriverExecutable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Rewrites an input string.</summary>
        /// <param name="input">Input string.</param>
        /// <returns>Rewritten string.</returns>
        string Rewrite(string input);
    }
}

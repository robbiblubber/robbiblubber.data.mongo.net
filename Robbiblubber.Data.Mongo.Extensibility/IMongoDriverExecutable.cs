﻿using System;



namespace Robbiblubber.Data.Mongo.Extensibility
{
    /// <summary>MongoDB driver methods or rewriters implementations implement this interface.</summary>
    public interface IMongoDriverExecutable
    {}
}

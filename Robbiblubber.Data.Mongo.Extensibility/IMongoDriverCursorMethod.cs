﻿using System;



namespace Robbiblubber.Data.Mongo.Extensibility
{
    /// <summary>MongoDB cursor method extensions implement this interface.</summary>
    public interface IMongoDriverCursorMethod: IMongoDriverMethod, IMongoDriverExecutable
    {}
}

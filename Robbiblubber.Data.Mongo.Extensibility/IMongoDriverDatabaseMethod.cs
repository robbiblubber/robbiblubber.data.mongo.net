﻿using System;



namespace Robbiblubber.Data.Mongo.Extensibility
{
    /// <summary>MongoDB database method extensions implement this interface.</summary>
    public interface IMongoDriverDatabaseMethod: IMongoDriverMethod, IMongoDriverExecutable
    {}
}

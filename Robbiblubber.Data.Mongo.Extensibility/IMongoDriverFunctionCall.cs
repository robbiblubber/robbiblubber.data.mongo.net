﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Data.Mongo.Extensibility
{
    /// <summary>Classes implementing this interface provide information about a MongoDB function call.</summary>
    public interface IMongoDriverFunctionCall
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the function name.</summary>
        string Name { get; }


        /// <summary>Gets the function arguments.</summary>
        string[] Arguments { get; }


        /// <summary>Gets the MongoCommand object on which the function has been called.</summary>
        IDbCommand Command { get;}


        /// <summary>Gets the MongoClient instance on which the function has been called.</summary>
        object Client { get; }


        /// <summary>Gets the database name for the function call.</summary>
        string Database { get; }


        /// <summary>Gets the collection name for the function call.</summary>
        string Collection { get; }


        /// <summary>Gets the input result table.</summary>
        MongoDriverResultTable Input { get; }
    }
}

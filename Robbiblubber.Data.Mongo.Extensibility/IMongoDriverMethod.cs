﻿using System;



namespace Robbiblubber.Data.Mongo.Extensibility
{
    /// <summary>MongoDB method extensions implement this interface.</summary>
    public interface IMongoDriverMethod: IMongoDriverExecutable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the names of the methods implemented in this class.</summary>
        string[] Methods { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        MongoDriverResultTable Execute(IMongoDriverFunctionCall func);
    }
}

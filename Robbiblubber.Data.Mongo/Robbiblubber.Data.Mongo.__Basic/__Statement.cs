﻿using System;
using System.Collections.Generic;

using MongoDB.Driver;
using MongoDB.Bson;

using Robbiblubber.Util;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo.__Basic
{
    /// <summary>This class represents a MongoDB statement.</summary>
    internal class __Statement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="cmd">Command object.</param>
        public __Statement(MongoCommand cmd)
        {
            try
            {
                string text = cmd.CommandText.Trim();

                Command = cmd;
                Database = (IMongoDatabase) cmd.Connection.Database;

                StringParser re;
                Collection = null;

                if(_IsDbMethod(text))
                {
                    re = new StringParser(text.Snip(1, '.'));
                }
                else
                {
                    Collection = text.Snip(1, 2, '.');
                    re = new StringParser(text.Snip(2, '.'));
                }

                while(re.More)
                {
                    Functions.Add(new __Func(re));
                }
            }
            catch(Exception ex) { throw new MongoMalformedStatementException("Statement malformed.", ex); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the collection the statement is executed on.</summary>
        public string Collection
        {
            get; private set;
        }


        /// <summary>Gets the functions in this statement.</summary>
        public List<__Func> Functions
        {
            get;
        } = new List<__Func>();


        /// <summary>Gets the parent command object.</summary>
        public MongoCommand Command
        {
            get; private set;
        }


        /// <summary>Gets the parent connetion.</summary>
        public MongoConnection Connection
        {
            get { return Command.Connection; }
        }


        /// <summary>Gets the MongoDB client.</summary>
        public IMongoClient Client
        {
            get { return Command.Connection._Client; }
        }


        /// <summary>Gets the operation database.</summary>
        public IMongoDatabase Database
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private properties                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the collection object.</summary>
        private IMongoCollection<BsonDocument> _Collection
        {
            get { return Database.GetCollection<BsonDocument>(Collection); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if a statement is a database method.</summary>
        /// <param name="stmt">Statement.</param>
        /// <returns>Returns TRUE if the statement is a database method, otherwise returns FALSE.</returns>
        private bool _IsDbMethod(string stmt)
        {
            return stmt.Substring(3).Before(".").EndsWith(")");
        }


        private void _ApplyParameters()
        {
            foreach(__Func i in Functions)
            {
                for(int k = 0; k < i.Arguments.Count; k++)
                {
                    if(Command.Parameters.Contains(i.Arguments[k])) { i.Arguments[k] = Command.Parameters[i.Arguments[k]]._ToString(); }
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Executes the Statement.</summary>
        /// <param name="c">Connection.</param>
        /// <returns>Result table.</returns>
        public MongoDriverResultTable Execute()
        {
            if(Functions.Count == 0) { throw new MongoMalformedStatementException("Statement malformed."); }
            _ApplyParameters();

            while(Functions[0].Name.ToLower() == "getsiblingdb")                // database selection
            {
                Database = Client.GetDatabase(Functions[0].Arguments[0].FromJsString());
                Functions.RemoveAt(0);

                if(Functions[0].Name.ToLower() == "aggregate") { Functions[0].Name = "dbaggregate"; }
            }

            if(Functions[0].Name.ToLower() == "getcollection")                  // collection selection
            {
                Collection = Functions[0].Arguments[0].FromJsString();
                Functions.RemoveAt(0);
            }

            MongoDriverResultTable rval = null;

            if(string.IsNullOrWhiteSpace(Collection))
            {
                foreach(IMongoDriverDatabaseMethod i in Connection._DatabaseMethods)
                {                                                               // database methods
                    try
                    {
                        if(i.Handles(Functions[0].Name)) { rval = i.Execute(new __FunctionCall(this, 0, null)); break; }
                    }
                    catch(Exception ex) { throw new MongoExecutionException(i.GetMethodName(Functions[0].Name) + "(): " + ex.Message, ex); }
                }

            }
            else
            {
                foreach(IMongoDriverCollectionMethod i in Connection._CollectionMethods)
                {                                                               // collection methods
                    try
                    {
                        if(i.Handles(Functions[0].Name)) { rval = i.Execute(new __FunctionCall(this, 0, null)); }
                    }
                    catch(Exception ex) { throw new MongoExecutionException(i.GetMethodName(Functions[0].Name) + "(): " + ex.Message, ex); }
                }
            }

            for(int i = 1; i < Functions.Count; i++)
            {
                foreach(IMongoDriverCursorMethod k in Connection._CursorMethods)
                {                                                               // cursor methods
                    try
                    {
                        if(k.Handles(Functions[i].Name)) { rval = k.Execute(new __CursorFunctionCall(this, i, rval)); }
                    }
                    catch(Exception ex) { throw new MongoExecutionException(k.GetMethodName(Functions[0].Name) + "(): " + ex.Message, ex); }
                }
            }

            if(rval == null) { throw new MongoUnknownFunctionException("Unknown function."); }
            
            return rval;
        }
    }
}

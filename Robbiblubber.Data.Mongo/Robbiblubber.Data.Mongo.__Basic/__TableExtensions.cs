﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo.__Basic
{
    /// <summary>This class provides extension methods for working with data tables.</summary>
    internal static class __TableExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a distinct result for a result table.</summary>
        /// <param name="tab">Table.</param>
        /// <returns>Result table.</returns>
        public static MongoDriverResultTable MakeDistinct(this MongoDriverResultTable tab)
        {
            List<DataRow> distinct = new List<DataRow>();
            foreach(DataRow i in tab.Rows)
            {
                if(!distinct.AlreadyContains(i)) { distinct.Add(i); }
            }

            return new MongoDriverResultTable(tab, distinct);
        }


        /// <summary>Returns if a set of data rows already contains a row with the same data.</summary>
        /// <param name="tab">Enumerable set of data rows.</param>
        /// <param name="row">Row.</param>
        /// <returns>Returns if a row with this data already exists in the result table.</returns>
        public static bool AlreadyContains(this IEnumerable<DataRow> tab, DataRow row)
        {
            foreach(DataRow i in tab)
            {
                if(i.ContentsEqual(row)) { return true; }
            }

            return false;
        }


        /// <summary>Returns data rows have equal contents.</summary>
        /// <param name="a">Data row.</param>
        /// <param name="b">Data row.</param>
        /// <returns>Returns TRUE if the rows have equal contents, otherwise returns FALSE.</returns>
        public static bool ContentsEqual(this DataRow a, DataRow b)
        {
            foreach(DataColumn i in a.Table.Columns)
            {
                if(a[i.ColumnName] == null)
                {
                    if(b[i.ColumnName] != null) { return false; }
                }
                else if(!a[i.ColumnName].Equals(b[i.ColumnName])) { return false; }
            }

            return true;
        }


        /// <summary>Aggregates values.</summary>
        /// <param name="method">Aggregation method.</param>
        /// <param name="ag">Aggregation object.</param>
        /// <param name="obj">Object.</param>
        /// <remarks>Aggregation result.</remarks>
        public static object Aggregate(this string method, object ag, object obj)
        {
            try
            {
                switch(method)
                {
                    case "min":
                        if(ag == null) { break; }
                        if(obj == null) { ag = null; break; }
                        if(obj is IComparable)
                        {
                            if(((IComparable) obj).CompareTo(ag) < 0) { ag = obj; }
                        }
                        else if(obj.ToString().CompareTo(ag.ToString()) < 0) { ag = obj; }
                        break;
                    case "max":
                        if(ag == null) { ag = obj; break; }
                        if(obj == null) { break; }
                        if(obj is IComparable)
                        {
                            if(((IComparable) obj).CompareTo(ag) > 0) { ag = obj; }
                        }
                        else if(obj.ToString().CompareTo(ag.ToString()) > 0) { ag = obj; }
                        break;
                    case "avg":
                    case "sum":
                        ag = ((double) ag) + (Convert.ToDouble(obj));
                        break;
                }
            }
            catch(Exception) {}

            return ag;
        }



        /// <summary>Returns a result table for a string.</summary>
        /// <param name="s">String.</param>
        /// <param name="colName">Column name</param>
        /// <param name="affected">Affected rows.</param>
        /// <returns>Table.</returns>
        public static MongoDriverResultTable ToTable(this string s, string colName = "result", int affected = 0)
        {
            MongoDriverResultTable rval = new MongoDriverResultTable(affected);
            rval.Columns.Add(colName, typeof(string));
            rval.Rows.Add(s);

            return rval;
        }


        /// <summary>Returns a result table for a set of objects.</summary>
        /// <param name="data">Data.</param>
        /// <param name="colName">Column name</param>
        /// <param name="affected">Affected rows.</param>
        /// <returns>Table.</returns>
        public static MongoDriverResultTable ToTable(this IEnumerable data, string colName = "result", int affected = 0)
        {
            MongoDriverResultTable rval = new MongoDriverResultTable(affected);
            rval.Columns.Add(colName, typeof(object));
            foreach(object i in data) { rval.Rows.Add(i); }

            return rval;
        }


        /// <summary>Returns a result table for an integer.</summary>
        /// <param name="s">String.</param>
        /// <param name="colName">Column name</param>
        /// <param name="affected">Affected rows.</param>
        /// <returns>Table.</returns>
        public static MongoDriverResultTable ToTable(this int i, string colName = "result", int affected = 0)
        {
            MongoDriverResultTable rval = new MongoDriverResultTable(affected);
            rval.Columns.Add(colName, typeof(int));
            rval.Rows.Add(i);

            return rval;
        }


        /// <summary>Gets the column key for an element.</summary>
        /// <param name="t">Table.</param>
        /// <param name="name">Element name.</param>
        /// <returns>Key.</returns>
        public static string GetColKey(this DataTable t, string name)
        {
            foreach(DataColumn i in t.Columns)
            {
                if(i.ColumnName.ToLower() == name.ToLower()) return i.ColumnName;
            }

            return null;
        }


        /// <summary>Returns if an element has a row key.</summary>
        /// <param name="t">Table.</param>
        /// <param name="name">Element name.</param>
        /// <returns>Returns TRUE if the element has a column, otherwise returns FALSE.</returns>
        public static bool HasColKey(this DataTable t, string name)
        {
            foreach(DataColumn i in t.Columns)
            {
                if(i.ColumnName.ToLower() == name.ToLower()) return true;
            }

            return false;
        }


        /// <summary>Gets a column for an element.</summary>
        /// <param name="t">Table.</param>
        /// <param name="name">Element name.</param>
        /// <returns>Column.</returns>
        public static DataColumn GetColumn(this DataTable t, string name)
        {
            foreach(DataColumn i in t.Columns)
            {
                if(i.ColumnName.ToLower() == name.ToLower()) return i;
            }

            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;
using MongoDB.Driver;



namespace Robbiblubber.Data.Mongo.__Basic
{
    /// <summary>This class provides extension methods to populate MongoDB option objects from BSON.</summary>
    internal static class __OptionsBuilderExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets options for database operations.</summary>
        /// <typeparam name="T">Option type.</typeparam>
        /// <param name="func">Function.</param>
        /// <param name="bse">Parameter base index.</param>
        /// <returns></returns>
        public static T GetOptions<T>(this IMongoDriverFunctionCall func, int bse = 0) where T: class
        {
            if(typeof(T) == typeof(InsertOneOptions))
            {
                if(func.Arguments.Length == 1) return null;

                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);
                InsertOneOptions rval = new InsertOneOptions();
                rval.BypassDocumentValidation = bson.GetVal(false, "bypass", "bypassvalidation", "bypassdocumentvalidation");

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(InsertManyOptions))
            {
                if(func.Arguments.Length == 1) return null;

                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);
                InsertManyOptions rval = new InsertManyOptions();
                rval.BypassDocumentValidation = bson.GetVal(false, "bypass", "bypassvalidation", "bypassdocumentvalidation");
                rval.IsOrdered = bson.GetVal(false, "ordered", "isordered");

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(UpdateOptions))
            {
                if(func.Arguments.Length == 2) return null;

                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);
                UpdateOptions rval = new UpdateOptions();

                string arrayfilters = bson.GetVal("", "arrayfilters");
                if(!string.IsNullOrWhiteSpace(arrayfilters))
                {
                    List<ArrayFilterDefinition<BsonDocument>> l = new List<ArrayFilterDefinition<BsonDocument>>();
                    foreach(BsonDocument i in arrayfilters.ParseArray())
                    {
                        l.Add(i);
                    }
                    rval.ArrayFilters = l;
                }

                BsonDocument col = bson.GetVal<BsonDocument>(null, "collation");
                if(col != null) rval.Collation = Collation.FromBsonDocument(col);

                rval.BypassDocumentValidation = bson.GetVal(false, "bypass", "bypassvalidation", "bypassdocumentvalidation");
                rval.IsUpsert = bson.GetVal(false, "upsert", "isupsert");
                rval.Hint = bson.GetVal(rval.Hint, "hint");
            }
            
            if(typeof(T) == typeof(DeleteOptions))
            {
                if(func.Arguments.Length == 1) return null;
                if(func.Name.ToLower() == "remove") return null;

                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);
                DeleteOptions rval = new DeleteOptions();
                
                BsonDocument col = bson.GetVal<BsonDocument>(null, "collation");
                if(col != null) rval.Collation = Collation.FromBsonDocument(col);

                rval.Hint = bson.GetVal(rval.Hint, "hint");

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(FindOptions))
            {
                if(func.Arguments.Length < 2) return null;
                if(func.Name.ToLower() == "select") return null;

                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);
                FindOptions rval = new FindOptions();

                BsonDocument col = bson.GetVal<BsonDocument>(null, "collation");
                if(col != null) rval.Collation = Collation.FromBsonDocument(col);

                rval.Hint = bson.GetVal(rval.Hint, "hint");

                rval.Hint = bson.GetVal(rval.Hint, "hint");
                rval.AllowDiskUse = bson.GetVal(rval.AllowDiskUse, "diskuse", "allowdiskuse");
                rval.AllowPartialResults = bson.GetVal(rval.AllowPartialResults, "partial", "allowpartial", "partialresults", "allowpartialresults");
                rval.BatchSize = bson.GetVal(rval.BatchSize, "batch", "batchsize");
                rval.Comment = bson.GetVal(rval.Comment, "comment");
                rval.Max = bson.GetVal(rval.Max, "max");
                rval.MaxAwaitTime = bson.GetVal(rval.MaxAwaitTime, "maxawait", "maxawaittime");
                rval.Min = bson.GetVal(rval.Min, "min");
                rval.NoCursorTimeout = bson.GetVal(rval.NoCursorTimeout, "notimeout", "nocursortimeout");
                rval.ReturnKey = bson.GetVal(rval.ReturnKey, "returnkey");
                rval.ShowRecordId = bson.GetVal(rval.ReturnKey, "recordid", "showrecordid");

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(CreateCollectionOptions))
            {
                if(func.Arguments.Length < 2) return null;

                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);
                CreateCollectionOptions rval = new CreateCollectionOptions();

                BsonDocument col = bson.GetVal<BsonDocument>(null, "collation");
                if(col != null) rval.Collation = Collation.FromBsonDocument(col);

                rval.Capped = bson.GetVal(rval.Capped, "capped");
                rval.MaxSize = bson.GetVal(rval.MaxSize, "size", "maxsize");
                rval.MaxDocuments = bson.GetVal(rval.MaxDocuments, "max", "maxdocuments");
                rval.StorageEngine = bson.GetVal(rval.StorageEngine, "storage", "engine", "storageengine");
                switch(bson.GetVal("", "validationlevel"))
                {
                    case "off": rval.ValidationLevel = DocumentValidationLevel.Off; break;
                    case "moderate": rval.ValidationLevel = DocumentValidationLevel.Moderate; break;
                    case "strict": rval.ValidationLevel = DocumentValidationLevel.Strict; break;
                }
                switch(bson.GetVal("", "validationaction"))
                {
                    case "error": rval.ValidationAction = DocumentValidationAction.Error; break;
                    case "warn":  rval.ValidationAction = DocumentValidationAction.Warn; break;
                }

                BsonDocument b2 = bson.GetVal<BsonDocument>(null, "indexoptiondefaults");
                if(b2 != null)
                {
                    rval.IndexOptionDefaults = new IndexOptionDefaults();
                    rval.IndexOptionDefaults.StorageEngine = b2;
                }

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(CreateViewOptions<BsonDocument>))
            {
                if(func.Arguments.Length < 4) return null;

                BsonDocument bson = BsonDocument.Parse(func.Arguments[3]);
                CreateViewOptions<BsonDocument> rval = new CreateViewOptions<BsonDocument>();

                BsonDocument col = bson.GetVal<BsonDocument>(null, "collation");
                if(col != null) rval.Collation = Collation.FromBsonDocument(col);
                
                return (T) (object) rval;
            }

            if(typeof(T) == typeof(AggregateOptions))
            {
                if(func.Arguments.Length < 2) return null;

                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);
                AggregateOptions rval = new AggregateOptions();

                BsonDocument col = bson.GetVal<BsonDocument>(null, "collation");
                if(col != null) rval.Collation = Collation.FromBsonDocument(col);

                rval.Hint = bson.GetVal(rval.Hint, "hint");

                rval.Hint = bson.GetVal(rval.Hint, "hint");
                rval.AllowDiskUse = bson.GetVal(rval.AllowDiskUse, "diskuse", "allowdiskuse");
                rval.BatchSize = bson.GetVal(rval.BatchSize, "batch", "batchsize");
                rval.Comment = bson.GetVal(rval.Comment, "comment");
                rval.MaxAwaitTime = bson.GetVal(rval.MaxAwaitTime, "maxawait", "maxawaittime");
                rval.MaxTime = bson.GetVal(rval.MaxTime, "maxtime", "maxtimems");
                rval.BypassDocumentValidation = bson.GetVal(false, "bypass", "bypassvalidation", "bypassdocumentvalidation");
                
                return (T) (object) rval;
            }

            if(typeof(T) == typeof(CountOptions))
            {
                if(func.Arguments.Length < 2) return null;

                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);
                CountOptions rval = new CountOptions();

                BsonDocument col = bson.GetVal<BsonDocument>(null, "collation");
                if(col != null) rval.Collation = Collation.FromBsonDocument(col);

                rval.Hint = bson.GetVal(rval.Hint, "hint");

                rval.Hint = bson.GetVal(rval.Hint, "hint");
                rval.MaxTime = bson.GetVal(rval.MaxTime, "maxtime", "maxtimems");
                rval.Limit = bson.GetVal(rval.Limit, "limit");
                rval.Skip = bson.GetVal(rval.Skip, "skip");

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(EstimatedDocumentCountOptions))
            {
                if(func.Arguments.Length < 1) return null;

                BsonDocument bson = BsonDocument.Parse(func.Arguments[0]);
                EstimatedDocumentCountOptions rval = new EstimatedDocumentCountOptions();

                rval.MaxTime = bson.GetVal(rval.MaxTime, "maxtime", "maxtimems");

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(CreateIndexOptions))
            {
                string data = "{}";
                if(bse == 0)
                {
                    if(func.Arguments.Length < 2) return null;
                    data = func.Arguments[1];
                }
                else
                {
                    if(func.Arguments.Length < 3) 
                    { 
                        data = "{ name: \"" + func.Arguments[0].FromJsString() + "\" }"; 
                    }
                    else { data = func.Arguments[2]; }
                }                

                BsonDocument bson = BsonDocument.Parse(data);
                CreateIndexOptions rval = new CreateIndexOptions();

                rval.Unique = bson.GetVal(rval.Unique, "unique");
                rval.Name = bson.GetVal(rval.Name, "name");
                rval.Sparse = bson.GetVal(rval.Sparse, "sparse");

                int? expire = bson.GetVal<int?>(null, "expire", "expireafter", "expireafterseconds");
                if(expire != null) { rval.ExpireAfter = new TimeSpan(0, 0, Convert.ToInt32(expire)); }

                rval.StorageEngine = bson.GetVal(rval.StorageEngine, "storage", "engine", "storageengine");

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(CreateOneIndexOptions))
            {
                if(func.Arguments.Length < (bse + 3)) return null;

                CreateOneIndexOptions rval = new CreateOneIndexOptions();
                rval.CommitQuorum = CreateIndexCommitQuorum.Create(func.Arguments[bse + 2].FromJsString());

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(DistinctOptions))
            {
                if(func.Arguments.Length < 3) return null;

                DistinctOptions rval = new DistinctOptions();
                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);

                BsonDocument col = bson.GetVal<BsonDocument>(null, "collation");
                if(col != null) rval.Collation = Collation.FromBsonDocument(col);

                rval.MaxTime = bson.GetVal(rval.MaxTime, "maxtime", "maxtimems");

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(FindOneAndDeleteOptions<BsonDocument>))
            {
                if(func.Arguments.Length < 2) return null;

                FindOneAndDeleteOptions<BsonDocument> rval = new FindOneAndDeleteOptions<BsonDocument>();
                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);

                BsonDocument col = bson.GetVal<BsonDocument>(null, "collation");
                if(col != null) rval.Collation = Collation.FromBsonDocument(col);

                rval.MaxTime = bson.GetVal(rval.MaxTime, "maxtime", "maxtimems");
                rval.Hint = bson.GetVal(rval.Hint, "hint");
                rval.Projection = bson.GetVal<BsonDocument>(null, "projection");
                rval.Sort = bson.GetVal<BsonDocument>(null, "sort");

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(FindOneAndReplaceOptions<BsonDocument>))
            {
                if(func.Arguments.Length < 2) return null;

                FindOneAndReplaceOptions<BsonDocument> rval = new FindOneAndReplaceOptions<BsonDocument>();
                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);

                BsonDocument col = bson.GetVal<BsonDocument>(null, "collation");
                if(col != null) rval.Collation = Collation.FromBsonDocument(col);

                rval.MaxTime = bson.GetVal(rval.MaxTime, "maxtime", "maxtimems");
                rval.Hint = bson.GetVal(rval.Hint, "hint");
                rval.Projection = bson.GetVal<BsonDocument>(null, "projection");
                rval.Sort = bson.GetVal<BsonDocument>(null, "sort");
                rval.IsUpsert = bson.GetVal(rval.IsUpsert, "upsert");
                rval.BypassDocumentValidation = bson.GetVal(false, "bypass", "bypassvalidation", "bypassdocumentvalidation");

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(ReplaceOptions))
            {
                if(func.Arguments.Length < 2) return null;

                ReplaceOptions rval = new ReplaceOptions();
                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);

                BsonDocument col = bson.GetVal<BsonDocument>(null, "collation");
                if(col != null) rval.Collation = Collation.FromBsonDocument(col);

                rval.Hint = bson.GetVal(rval.Hint, "hint");
                rval.IsUpsert = bson.GetVal(rval.IsUpsert, "upsert");
                rval.BypassDocumentValidation = bson.GetVal(false, "bypass", "bypassvalidation", "bypassdocumentvalidation");

                return (T) (object) rval;
            }

            if(typeof(T) == typeof(FindOneAndUpdateOptions<BsonDocument>))
            {
                if(func.Arguments.Length < 2) return null;

                FindOneAndUpdateOptions<BsonDocument> rval = new FindOneAndUpdateOptions<BsonDocument>();
                BsonDocument bson = BsonDocument.Parse(func.Arguments[1]);

                string arrayfilters = bson.GetVal("", "arrayfilters");
                if(!string.IsNullOrWhiteSpace(arrayfilters))
                {
                    List<ArrayFilterDefinition<BsonDocument>> l = new List<ArrayFilterDefinition<BsonDocument>>();
                    foreach(BsonDocument i in arrayfilters.ParseArray())
                    {
                        l.Add(i);
                    }
                    rval.ArrayFilters = l;
                }

                BsonDocument col = bson.GetVal<BsonDocument>(null, "collation");
                if(col != null) rval.Collation = Collation.FromBsonDocument(col);

                rval.MaxTime = bson.GetVal(rval.MaxTime, "maxtime", "maxtimems");
                rval.Hint = bson.GetVal(rval.Hint, "hint");
                rval.Projection = bson.GetVal<BsonDocument>(null, "projection");
                rval.Sort = bson.GetVal<BsonDocument>(null, "sort");
                rval.IsUpsert = bson.GetVal(rval.IsUpsert, "upsert");
                rval.BypassDocumentValidation = bson.GetVal(false, "bypass", "bypassvalidation", "bypassdocumentvalidation");
                
                return (T) (object) rval;
            }

            if(typeof(T) == typeof(RenameCollectionOptions))
            {
                if(func.Arguments.Length < 2) return null;

                RenameCollectionOptions rval = new RenameCollectionOptions();
                rval.DropTarget = ((func.Arguments[1].ToLower() == "true") || (func.Arguments[1] == "1"));

                return (T) (object) rval;
            }

            return null;
        }
    }
}

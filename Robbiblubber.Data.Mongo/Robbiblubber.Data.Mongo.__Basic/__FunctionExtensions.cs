﻿using System;
using MongoDB.Bson;
using MongoDB.Driver;

using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo.__Basic
{
    /// <summary>This class provides extension methods for the IMongoDriverFunctionCall interface.</summary>
    internal static class __FunctionExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the database instance.</summary>
        /// <param name="func">Function call.</param>
        /// <returns>Database instance.</returns>
        public static IMongoDatabase GetDatabase(this IMongoDriverFunctionCall func)
        {
            return ((MongoConnection) func.Command.Connection)._Database;
        }


        /// <summary>Gets the database instance.</summary>
        /// <param name="func">Function call.</param>
        /// <returns>Database instance.</returns>
        public static IMongoCollection<BsonDocument> GetCollection(this IMongoDriverFunctionCall func)
        {
            return ((MongoConnection) func.Command.Connection)._Database.GetCollection<BsonDocument>(func.Collection);
        }


        /// <summary>Gets the MongoDB client.</summary>
        /// <param name="func">Function call.</param>
        /// <returns>Database instance.</returns>
        public static IMongoClient GetClient(this IMongoDriverFunctionCall func)
        {
            return ((MongoConnection) func.Command.Connection)._Client;
        }


        /// <summary>Returns if a method implementation handles a function call.</summary>
        /// <param name="method">Method implementation.</param>
        /// <param name="methodName">Function name.</param>
        /// <returns></returns>
        public static bool Handles(this IMongoDriverMethod method, string methodName)
        {
            foreach(string i in method.Methods)
            {
                if(i.ToLower() == methodName.ToLower()) { return true; }
            }

            return false;
        }


        /// <summary>Returns the well-formatted method name.</summary>
        /// <param name="method">Method.</param>
        /// <param name="methodName">Method name.</param>
        /// <returns>Method name.</returns>
        public static string GetMethodName(this IMongoDriverMethod method, string methodName)
        {
            foreach(string i in method.Methods)
            {
                if(i.ToLower() == methodName.ToLower()) { return i; }
            }

            return "unknown?";
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.IO;

using Robbiblubber.Util;
using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo.__Basic
{    
    /// <summary>This class provides extension methods for parsing BSON documents.</summary>
    internal static class __BsonExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Returns the inner string value of a JS string token.</summary>
        /// <param name="s">JS String.</param>
        /// <returns>String.</returns>
        public static string FromJsString(this string s)
        {
            return s.Trim().Trim('\'', '\"');
        }


        /// <summary>Gets a </summary>
        /// <param name="doc"></param>
        /// <param name="field"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public static T GetVal<T>(this BsonDocument doc, T @default, params string[] fields)
        {
            BsonValue rval = null;
            foreach(BsonElement i in doc)
            {
                foreach(string field in fields)
                {
                    if(i.Name.ToLower() == field.ToLower()) { rval = i.Value; break; }
                }
            }
            if(rval == null) { return @default; }

            if((typeof(T) == typeof(bool)) || (typeof(T) == typeof(bool?)))
            {
                return (T) (object) rval.AsBoolean;
            }

            if((typeof(T) == typeof(int)) || (typeof(T) == typeof(int?)))
            {
                return (T) (object) rval.AsInt32;
            }

            if(typeof(T) == typeof(string))
            {
                return (T) (object) rval.AsString;
            }
            
            if(typeof(T) == typeof(BsonValue))
            {
                return (T) (object) rval;
            }
            
            if(typeof(T) == typeof(BsonDocument))
            {
                return (T) (object) rval.AsBsonDocument;
            }

            if((typeof(T) == typeof(TimeSpan)) || (typeof(T) == typeof(TimeSpan?)))
            {
                return (T) (object) new TimeSpan(rval.AsInt64 * TimeSpan.TicksPerMillisecond);
            }

            throw new NotSupportedException();
        }


        /// <summary>Parses an array of BSON documents.</summary>
        /// <param name="s">String.</param>
        /// <returns>List of BSON documents.</returns>
        public static List<BsonDocument> ParseArray(this string s)
        {
            List<BsonDocument> rval = new List<BsonDocument>();

            if(s.Trim().StartsWith("["))
            {
                List<string> docs = new List<string>();

                StringParser re = new StringParser(s.Trim().Substring(1, s.Trim().Length - 2));

                string data = "";
                bool inString = false;
                bool inStringSingle = false;

                int bracks = 0;

                while(re.More)
                {
                    re.ReadPart("\"", "'", "{", "}", ",");

                    if(inString)
                    {
                        data += re.CurrentPart + re.CurrentDelimiter;
                        if((re.CurrentDelimiter == "\"") && (re.Preceding != '\\')) { inString = false; }
                        continue;
                    }
                    if(inStringSingle)
                    {
                        data += re.CurrentPart + re.CurrentDelimiter;
                        if((re.CurrentDelimiter == "\'") && (re.Preceding != '\\')) { inStringSingle = false; }
                        continue;
                    }

                    if(re.CurrentDelimiter == "\"")
                    {
                        data += re.CurrentPart + re.CurrentDelimiter;
                        inString = true;
                        continue;
                    }
                    if(re.CurrentDelimiter == "'")
                    {
                        data += re.CurrentPart + re.CurrentDelimiter;
                        inStringSingle = true;
                        continue;
                    }

                    if(re.CurrentDelimiter == "{")
                    {
                        data += re.CurrentPart + re.CurrentDelimiter;
                        bracks++;
                        continue;
                    }

                    if(re.CurrentDelimiter == "}")
                    {
                        data += re.CurrentPart + re.CurrentDelimiter;
                        bracks--;
                        continue;
                    }

                    if(bracks > 0)
                    {
                        data += re.CurrentPart + re.CurrentDelimiter;
                        continue;
                    }

                    if(re.CurrentDelimiter == ",")
                    {
                        data += re.CurrentPart;
                        docs.Add(data.Trim());
                        data = "";
                        continue;
                    }
                }
                if(!string.IsNullOrWhiteSpace(data)) { docs.Add(data.Trim()); }

                foreach(string i in docs)
                {
                    if(!string.IsNullOrWhiteSpace(i)) { rval.Add(BsonDocument.Parse(i)); }
                }
            }
            else { rval.Add(BsonDocument.Parse(s)); }

            return rval;
        }


        /// <summary>Returns a table for an object.</summary>
        /// <param name="o">Object.</param>
        /// <returns>Table.</returns>
        public static MongoDriverResultTable ParseTable(this object o)
        {
            if(o is BsonDocument) { o = ((BsonDocument) o).ToDictionary(); }

            MongoDriverResultTable rval = new MongoDriverResultTable();

            if(o is IDictionary)
            {
                foreach(object i in ((IDictionary) o).Keys)
                {
                    rval.Columns.Add(i.ToString());
                    rval.Rows.Add(((IDictionary) o)[i]);
                }
            }
            else
            {
                rval.Columns.Add("Values");

                if(o is IEnumerable)
                {
                    foreach(object i in ((IEnumerable) o)) { rval.Rows.Add(i); }
                }
                else
                {
                    rval.Rows.Add(o);
                }
            }

            return rval;
        }


        /// <summary>Gets the table contents as BSON documents.</summary>
        /// <param name="tab">Result table.</param>
        /// <returns>List of BSON documents.</returns>
        public static List<BsonDocument> ToBson(this MongoDriverResultTable tab)
        {
            List<BsonDocument> rval = new List<BsonDocument>();
            if(tab.Columns.Count == 1)
            {
                if(tab.Columns[0].DataType == typeof(BsonDocument))
                {
                    foreach(DataRow i in tab.Rows) { rval.Add((BsonDocument) i[0]); }
                }
                else
                {
                    foreach(DataRow i in tab.Rows) { rval.Add(BsonDocument.Parse(i[0].ToString())); }
                }
            }
            else
            {
                foreach(DataRow i in tab.Rows)
                {
                    BsonDocument d = new BsonDocument();
                    foreach(DataColumn k in tab.Columns)
                    {
                        if(i[k.ColumnName] is DBNull)
                        {
                            d.Add(new BsonElement(k.ColumnName, BsonNull.Value));
                        }
                        else { d.Add(new BsonElement(k.ColumnName, BsonValue.Create(i[k.ColumnName]))); }
                    }
                    rval.Add(d);
                }
            }

            return rval;
        }


        /// <summary>Returns a table of BSON documents.</summary>
        /// <param name="docs">Documents.</param>
        /// <returns>Table.</returns>
        public static MongoDriverResultTable ToBsonTable(this IEnumerable<BsonDocument> docs)
        {
            MongoDriverResultTable rval = new MongoDriverResultTable();
            rval.Columns.Add("Documents", typeof(BsonDocument));

            foreach(BsonDocument i in docs) { rval.Rows.Add(i); }
            return rval;
        }


        /// <summary>Returns a table of BSON documents.</summary>
        /// <param name="docs">Documents.</param>
        /// <param name="doc">Number of records modified.</param>
        /// <returns>Table.</returns>
        public static MongoDriverResultTable ToBsonTable(this BsonDocument doc, int modifiedCount = 0)
        {
            MongoDriverResultTable rval = new MongoDriverResultTable(modifiedCount);
            rval.Columns.Add("Documents", typeof(BsonDocument));

            rval.Rows.Add(doc);
            return rval;
        }


        /// <summary>Formats a BSON document.</summary>
        /// <param name="bson">BSON docuemnt.</param>
        /// <returns>Formatted output.</returns>
        public static string Pretty(this BsonDocument bson)
        {
            JsonWriterSettings settings = new JsonWriterSettings();
            settings.Indent = true;

            return bson.ToJson(settings);
        }


        /// <summary>Formats a JSON document.</summary>
        /// <param name="bson">JSON docuemnt.</param>
        /// <returns>Formatted output.</returns>
        public static string Pretty(this string json)
        {
            return BsonDocument.Parse(json).Pretty();
        }


        /// <summary>Returns a table of JSON documents.</summary>
        /// <param name="docs">Documents.</param>
        /// <returns>Table.</returns>
        public static MongoDriverResultTable ToJsonTable(this IEnumerable<BsonDocument> docs, bool pretty = false)
        {
            MongoDriverResultTable rval = new MongoDriverResultTable();
            rval.Columns.Add("Documents", typeof(string));

            foreach(BsonDocument i in docs)
            {
                rval.Rows.Add(pretty ? i.Pretty() : i.ToJson());
            }
            return rval;
        }


        /// <summary>Parses a BSON document cursor to a data table.</summary>
        /// <param name="docs">Documents.</param>
        /// <param name="sel">Selection.</param>
        /// <returns>Table.</returns>
        public static MongoDriverResultTable ToTable(this IEnumerable<BsonDocument> docs, List<string> sel = null)
        {
            MongoDriverResultTable rval = new MongoDriverResultTable();
            List<string> lsel = new List<string>();
            if(sel == null) { sel = new List<string>(); }

            if(!sel.Contains("*"))
            {
                foreach(string i in sel) 
                { 
                    rval.Columns.Add(i);
                    lsel.Add(i.ToLower());
                }
            }
            
            if(lsel.Count == 0) { lsel.Add("*"); }

            foreach(BsonDocument i in docs)
            {
                DataRow r = rval.NewRow();

                foreach(BsonElement k in i.Elements)
                {
                    object v = k.Value;
                                        
                    if(!rval.HasColKey(k.Name))
                    {
                        if(lsel.Contains("*")) rval.Columns.Add(k.Name);
                    }

                    if(rval.HasColKey(k.Name))
                    {
                        r[rval.GetColKey(k.Name)] = v;
                    }
                }
                rval.Rows.Add(r);
            }

            return rval;
        }


        /// <summary>Parses a BSON document cursor to a data table.</summary>
        /// <param name="cur">Cursor.</param>
        /// <returns></returns>
        public static MongoDriverResultTable ToTable(this IAsyncCursor<BsonDocument> cur)
        {
            List<BsonDocument> l = new List<BsonDocument>();
            while(cur.MoveNext())
            {
                foreach(BsonDocument i in cur.Current)
                {
                    l.Add(i);
                }
            }
            cur.Dispose();

            return ToTable(l, null);
        }


        /// <summary>Parses an array of dictionaries to a data table.</summary>
        /// <param name="data">Data.</param>
        /// <returns>Data table.</returns>
        public static MongoDriverResultTable __ToTable(this Dictionary<string, object>[] data)
        {
            MongoDriverResultTable rval = new MongoDriverResultTable();
            rval.Columns.Add("Values", typeof(Dictionary<string, object>));

            foreach(Dictionary<string, object> i in data) { rval.Rows.Add(i); }

            return rval;
        }


        /// <summary>Parses a dirctionary to a data table.</summary>
        /// <param name="data">Data.</param>
        /// <returns>Data table.</returns>
        public static MongoDriverResultTable __ToTable(this Dictionary<string, object> data)
        {
            MongoDriverResultTable rval = new MongoDriverResultTable();
            rval.Columns.Add("Keys", typeof(string));
            rval.Columns.Add("Values");

            foreach(KeyValuePair<string, object> i in data)
            {
                rval.Rows.Add(i.Key, i.Value);
            }

            return rval;
        }


        /// <summary>Converts a BSON docuemnt array to an array of dictionaries.</summary>
        /// <param name="array">Array of BSON documents.</param>
        /// <returns>Array of dictionaries.</returns>
        public static Dictionary<string, object>[] __ToDictionaryArray(this BsonDocument[] array)
        {
            List<Dictionary<string, object>> rval = new List<Dictionary<string, object>>();

            foreach(BsonDocument i in array) { rval.Add(i.ToDictionary()); }

            return rval.ToArray();
        }
    }
}

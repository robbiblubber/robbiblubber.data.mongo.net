﻿using System;
using System.Collections.Generic;
using System.Data;

using MongoDB.Bson;



namespace Robbiblubber.Data.Mongo.__Basic
{
    /// <summary>This class implements a row comparer.</summary>
    internal class __RowComparer: IComparer<Tuple<DataRow, int>>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Fields.</summary>
        private List<Tuple<string, bool>> _Fields;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="fields">Fields.</param>
        public __RowComparer(List<Tuple<string, bool>> fields)
        {
            _Fields = fields;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IComparer<DataRow>                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares two objects.</summary>
        /// <param name="x">Object.</param>
        /// <param name="y">Object.</param>
        /// <returns>Returns if an object is less than, equal, or greater than the other.</returns>
        public int Compare(Tuple<DataRow, int> x, Tuple<DataRow, int> y)
        {
            int rval = 0;

            if(x.Item1[0] is BsonDocument)
            {
                foreach(Tuple<string, bool> i in _Fields)
                {
                    try
                    {
                        if(!((BsonDocument) x.Item1[0]).Contains(i.Item1)) continue;

                        if(!((BsonDocument) y.Item1[0]).Contains(i.Item1))
                        {
                            rval = -1;
                        }
                        else
                        {
                            rval = ((BsonDocument) x.Item1[0])[i.Item1].ToString().CompareTo(((BsonDocument) y.Item1[0])[i.Item1].ToString());
                        }

                        if(rval != 0) { return (i.Item2 ? rval : -rval); }
                    }
                    catch(Exception) {}
                }
            }
            else
            {
                foreach(Tuple<string, bool> i in _Fields)
                {
                    try
                    {
                        if(!x.Item1.Table.Columns.Contains(i.Item1)) continue;

                        if(x.Item1[i.Item1] == null)
                        {
                            rval = ((y.Item1[i.Item1] == null) ? 0 : -1);
                        }
                        else if(y.Item1[i.Item1] == null)
                        {
                            rval = 1;
                        }
                        else if(x.Item1[i.Item1] is IComparable)
                        {
                            rval = ((IComparable) x.Item1[i.Item1]).CompareTo(y.Item1[i.Item1]);
                        }
                        else { rval = x.Item1[i.Item1].ToString().CompareTo(y.Item1[i.Item1].ToString()); }

                        if(rval != 0) { return (i.Item2 ? rval : -rval); }
                    }
                    catch(Exception) {}
                }
            }

            return x.Item2.CompareTo(y.Item2);
        }
    }
}

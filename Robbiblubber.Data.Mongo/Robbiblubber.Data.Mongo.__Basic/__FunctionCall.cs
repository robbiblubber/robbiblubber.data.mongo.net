﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo.__Basic
{
    /// <summary>This class provides a IMongoDriveFunctionCall implementation for statements.</summary>
    internal class __FunctionCall: IMongoDriverFunctionCall
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Statement.</summary>
        internal __Statement _Statement;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="func">Function.</param>
        public __FunctionCall(__Func func)
        {
            Name = func.Name;
            Arguments = func.Arguments.ToArray();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="name">Function name.</param>
        /// <param name="args">Arguments.</param>
        public __FunctionCall(string name, IEnumerable<string> args)
        {
            Name = name;
            Arguments = args.ToArray();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="stmt">Statement.</param>
        /// <param name="i">Function index.</param>
        /// <param name="input">Input table.</param>
        public __FunctionCall( __Statement stmt, int i, MongoDriverResultTable input)
        {
            _Statement = stmt;

            Name = stmt.Functions[i].Name;
            Arguments = stmt.Functions[i].Arguments.ToArray();

            Command = stmt.Command;
            Client = stmt.Command.Connection.Client;
            Database = stmt.Database.DatabaseNamespace.DatabaseName;
            Collection = stmt.Collection;
            Input = input;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverFunctionData                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the function name.</summary>
        public string Name
        {
            get; protected set;
        }


        /// <summary>Gets the function arguments.</summary>
        public string[] Arguments
        {
            get; protected set;
        }


        /// <summary>Gets the MongoCommand object on which the function has been called.</summary>
        public IDbCommand Command 
        { 
            get; protected set;
        }


        /// <summary>Gets the MongoClient instance on which the function has been called.</summary>
        public object Client
        {
            get; protected set;
        }


        /// <summary>Gets the database name for the function call.</summary>
        public string Database
        {
            get; protected set;
        }


        /// <summary>Gets the collection name for the function call.</summary>
        public string Collection
        {
            get; protected set;
        }


        /// <summary>Gets the input result table.</summary>
        public MongoDriverResultTable Input
        {
            get; protected set;
        }
    }
}

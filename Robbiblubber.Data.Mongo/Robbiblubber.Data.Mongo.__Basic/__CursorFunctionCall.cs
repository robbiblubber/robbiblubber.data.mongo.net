﻿using System;

using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo.__Basic
{
    /// <summary>This class provides a IMongoDriveFunctionCall implementation for cursors.</summary>
    internal class __CursorFunctionCall: __FunctionCall
    {
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="stmt">Statement.</param>
        /// <param name="i">Function index.</param>
        /// <param name="input">Input table.</param>
        public __CursorFunctionCall(__Statement stmt, int i, MongoDriverResultTable input): base(stmt, i, input)
        {
            Collection = null;
        }
    }
}

﻿using System;
using System.Collections.Generic;

using Robbiblubber.Util;



namespace Robbiblubber.Data.Mongo.__Basic
{
    /// <summary>This class represents a MongoDB function.</summary>
    internal class __Func
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="name">Function name.</param>
        /// <param name="args">Arguments.</param>
        public __Func(string name, IEnumerable<string> args)
        {
            Name = name;
            Arguments.AddRange(args);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="re">String parser.</param>
        public __Func(StringParser re)
        {
            string data = "";
            bool inString = false;
            bool inStringSingle = false;

            int bracks = 0;

            while(re.More)
            {
                re.ReadPart("(", ")", "\"", "'", "[", "{", "}", "]", ",");

                if(inString) 
                {
                    data += re.CurrentPart + re.CurrentDelimiter;
                    if((re.CurrentDelimiter == "\"") && (re.Preceding != '\\')) { inString = false; }
                    continue;
                }
                if(inStringSingle)
                {
                    data += re.CurrentPart + re.CurrentDelimiter;
                    if((re.CurrentDelimiter == "\'") && (re.Preceding != '\\')) { inStringSingle = false; }
                    continue;
                }

                if(re.CurrentDelimiter == "\"")
                {
                    data += re.CurrentPart + re.CurrentDelimiter;
                    inString = true;
                    continue;
                }
                if(re.CurrentDelimiter == "'")
                {
                    data += re.CurrentPart + re.CurrentDelimiter;
                    inStringSingle = true;
                    continue;
                }

                if((re.CurrentDelimiter == "{") || (re.CurrentDelimiter == "["))
                {
                    data += re.CurrentPart + re.CurrentDelimiter;
                    bracks++;
                    continue;
                }

                if((re.CurrentDelimiter == "}") || (re.CurrentDelimiter =="]"))
                {
                    data += re.CurrentPart + re.CurrentDelimiter;
                    bracks--;
                    continue;
                }

                if(bracks > 0) 
                {
                    data += re.CurrentPart + re.CurrentDelimiter;
                    continue;
                }

                if(re.CurrentDelimiter == "(")
                {
                    data += re.CurrentPart;
                    Name = data.TrimStart('.');
                    data = "";
                    continue;
                }

                if(re.CurrentDelimiter == ")")
                {
                    data += re.CurrentPart;
                    if(!string.IsNullOrWhiteSpace(data)) { Arguments.Add(data.Trim()); }
                    break;
                }

                if(re.CurrentDelimiter == ",")
                {
                    data += re.CurrentPart;
                    Arguments.Add(data.Trim());
                    data = "";
                    continue;
                }
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the function name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the function arguments.</summary>
        public List<string> Arguments
        {
            get;
        } = new List<string>();
    }
}

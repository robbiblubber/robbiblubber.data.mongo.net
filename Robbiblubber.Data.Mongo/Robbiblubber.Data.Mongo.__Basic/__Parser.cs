﻿using System;

using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo.__Basic
{
    /// <summary>This class implements a MongoDB parser.</summary>
    internal sealed class __Parser
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="c">Connection.</param>
        public __Parser()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Parses a command string.</summary>
        /// <param name="cmd">Command object.</param>
        /// <returns>Data table.</returns>
        public MongoDriverResultTable Parse(MongoCommand cmd)
        {
            if(cmd.CommandText.Trim().ToLower() == "db")
            {
                cmd.CommandText = "db.getName()";
            }

            if(cmd.CommandText.Trim().ToLower().StartsWith("use "))
            {
                cmd.Connection.ChangeDatabase(cmd.CommandText.Trim().Substring(4).Trim());
                return MongoDriverResultTable.EMPTY;
            }
            if(cmd.CommandText.Trim().ToLower() == "show databases")
            {
                return cmd.Connection._Client.ListDatabases().ToTable();
            }

            if(cmd.CommandText.Trim().ToLower() == "show collections")
            {
                return cmd.Connection._Database.ListCollections().ToTable();
            }

            if(cmd.CommandText.Trim().StartsWith("db."))
            {
                __Statement st = new __Statement(cmd);
                return st.Execute();
            }

            throw new MongoUnitellegibleStatementException("Statement unintelligible.");
        }
    }
}

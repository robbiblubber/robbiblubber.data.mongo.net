﻿using System;



namespace Robbiblubber.Data.Mongo.Exceptions
{
    /// <summary>This class implements an exception that is thrown when a statement could not be parsed to a set of functions.</summary>
    public class MongoMalformedStatementException: MongoExecutionException
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MongoMalformedStatementException(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        public MongoMalformedStatementException(string message): base(message)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">A previous exception that caused the exception to be thrown.</param>
        public MongoMalformedStatementException(string message, Exception innerException): base(message, innerException)
        {}
    }
}

﻿using System;



namespace Robbiblubber.Data.Mongo.Exceptions
{
    /// <summary>This class implements an exception that is thrown when invalid arguments were passed to a function.</summary>
    public class MongoArgumentException: MongoException
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MongoArgumentException(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        public MongoArgumentException(string message): base(message)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">A previous exception that caused the exception to be thrown.</param>
        public MongoArgumentException(string message, Exception innerException): base(message, innerException)
        {}
    }
}

﻿using System;



namespace Robbiblubber.Data.Mongo.Exceptions
{
    /// <summary>This class implements the MongoDB driver base exception.</summary>
    public class MongoException: Exception
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public MongoException(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        public MongoException(string message): base(message)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">A previous exception that caused the exception to be thrown.</param>
        public MongoException(string message, Exception innerException): base(message, innerException)
        {}
    }
}

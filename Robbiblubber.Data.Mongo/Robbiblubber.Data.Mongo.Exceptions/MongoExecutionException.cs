﻿using System;



namespace Robbiblubber.Data.Mongo.Exceptions
{
    /// <summary>This class implements an exception that is thrown when an execution has failed.</summary>
    public class MongoExecutionException: MongoException
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MongoExecutionException(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        public MongoExecutionException(string message): base(message)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">A previous exception that caused the exception to be thrown.</param>
        public MongoExecutionException(string message, Exception innerException): base(message, innerException)
        {}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robbiblubber.Data.Mongo.Exceptions
{
    /// <summary>This class implements an exception that is thrown when an unknown function has been called.</summary>
    public class MongoUnknownFunctionException: MongoException
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MongoUnknownFunctionException(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        public MongoUnknownFunctionException(string message): base(message)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">A previous exception that caused the exception to be thrown.</param>
        public MongoUnknownFunctionException(string message, Exception innerException): base(message, innerException)
        {}
    }
}

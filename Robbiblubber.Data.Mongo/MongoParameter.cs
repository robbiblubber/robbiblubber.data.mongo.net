﻿using System;
using System.Collections;
using System.Data;



namespace Robbiblubber.Data.Mongo
{
    public class MongoParameter: IDbDataParameter, IDataParameter
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public MongoParameter()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <param name="value">Parameter value.</param>
        public MongoParameter(string parameterName, object value)
        {
            ParameterName = parameterName;
            Value = value;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal methods                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Returns a JSON string representation of the value.</summary>
        /// <returns>JSON string.</returns>
        internal string _ToString()
        {
            if(Value is IEnumerable)
            {
                string rval = "[";
                foreach(object i in (IEnumerable) Value)
                {
                    if(rval.Length > 1) { rval += ","; }
                    rval += i.ToString();
                }
            }

            return Value.ToString();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDataParameter                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the database type.</summary>
        DbType IDataParameter.DbType
        {
            get; set;
        }


        /// <summary>Gets or sets the parameter direction.</summary>
        ParameterDirection IDataParameter.Direction
        {
            get;  set;
        }


        /// <summary>Gets if the parameter is nullable.</summary>
        bool IDataParameter.IsNullable
        {
            get { return true; }
        }


        /// <summary>Gets or sets the parameter name.</summary>
        public string ParameterName
        {
            get; set;
        }


        /// <summary>Gets or sets the source column name.</summary>
        string IDataParameter.SourceColumn
        {
            get; set;
        }


        /// <summary>Gets or sets the source version.</summary>
        DataRowVersion IDataParameter.SourceVersion
        {
            get; set;
        }


        /// <summary>Gets or sets the value.</summary>
        public object Value
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbDataParameter                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the parameter precision.</summary>
        byte IDbDataParameter.Precision
        {
            get; set;
        }


        /// <summary>Gets or sets the parameter scale.</summary>
        byte IDbDataParameter.Scale
        {
            get; set;
        }


        /// <summary>Gets or sets the parameter size.</summary>
        int IDbDataParameter.Size
        {
            get; set;
        }
    }
}

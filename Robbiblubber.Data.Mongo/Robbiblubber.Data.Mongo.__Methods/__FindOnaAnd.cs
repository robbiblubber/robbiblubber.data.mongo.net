﻿using System;
using System.Collections.Generic;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;
using MongoDB.Driver;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the findOneAndDelete(), findOneAndReplace(), findOneAndUpdate methods.</summary>
    internal class __FindOneAnd: IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "findOneAndDelete", "findOneAndReplace", "findOneAndUpdate" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            BsonDocument filter;
            MongoDriverResultTable rval = new MongoDriverResultTable();

            try
            {
                if(func.Arguments.Length == 0)
                {
                    filter = BsonDocument.Parse("{}");
                }
                else { filter = BsonDocument.Parse(func.Arguments[0]); }
            }
            catch(Exception ex) { throw new MongoArgumentException("Invalid arguments.", ex); }

            if(func.Name.ToLower() == "findoneanddelete")
            {
                BsonDocument result = func.GetCollection().FindOneAndDelete(filter, func.GetOptions<FindOneAndDeleteOptions<BsonDocument>>());
                rval = ((result == null) ? new MongoDriverResultTable() : result.ToBsonTable(1));
            }
            else if(func.Name.ToLower() == "findoneandreplace")
            {
                if(func.Arguments.Length < 2) { throw new MongoArgumentException("Missing arguments."); }

                BsonDocument result = func.GetCollection().FindOneAndReplace(filter, BsonDocument.Parse(func.Arguments[1]), func.GetOptions<FindOneAndReplaceOptions<BsonDocument>>());
                rval = ((result == null) ? new MongoDriverResultTable() : result.ToBsonTable(1));
            }
            else if(func.Name.ToLower() == "findoneandupdate")
            {
                if(func.Arguments.Length < 2) { throw new MongoArgumentException("Missing arguments."); }

                BsonDocument result = func.GetCollection().FindOneAndReplace(filter, BsonDocument.Parse(func.Arguments[1]), func.GetOptions<FindOneAndReplaceOptions<BsonDocument>>());
                rval = ((result == null) ? new MongoDriverResultTable() : result.ToBsonTable(1));
            }

            return rval;
        }
    }
}

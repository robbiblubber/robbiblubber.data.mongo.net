﻿using System;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;
using System.Collections.Generic;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the select() method.</summary>
    internal class __Select: IMongoDriverCursorMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        internal static MongoDriverResultTable __Execute(IMongoDriverFunctionCall func)
        {
            List<string> sel = new List<string>();

            if(func.Arguments.Length > 0)
            {
                foreach(string k in func.Arguments)
                {
                    string s = k.FromJsString();
                    if(!string.IsNullOrWhiteSpace(s)) { sel.Add(s); }
                }
            }
            if(sel.Count == 0) { sel.Add("*"); }

            return func.Input.ToBson().ToTable(sel);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "select" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            return __Execute(func);
        }
    }
}

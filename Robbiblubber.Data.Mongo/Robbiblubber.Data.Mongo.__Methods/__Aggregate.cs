﻿using System;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;
using MongoDB.Driver;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the aggregate() method.</summary>
    internal class __Aggregate: IMongoDriverDatabaseMethod, IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "aggregate" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(string.IsNullOrWhiteSpace(func.Collection))
            {
                if(func.Arguments.Length < 3) { throw new MongoArgumentException("Argument missing."); }

                try
                {
                    return func.GetDatabase().Aggregate<BsonDocument>(func.Arguments[0].ParseArray(), func.GetOptions<AggregateOptions>()).ToEnumerable().ToBsonTable();
                }
                catch(Exception ex) { throw new MongoExecutionException(ex.Message, ex); }
            }
            else
            {
                if(func.Arguments.Length < 1) { throw new MongoArgumentException("Arguments missing."); }

                try
                {
                    return func.GetCollection().Aggregate<BsonDocument>(func.Arguments[0].ParseArray(), func.GetOptions<AggregateOptions>()).ToEnumerable().ToBsonTable();
                }
                catch(Exception ex) { throw new MongoExecutionException(ex.Message, ex); }
            }
        }
    }
}

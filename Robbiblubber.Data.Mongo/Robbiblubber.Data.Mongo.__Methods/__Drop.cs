﻿using System;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the drop(), dropDatabase(), dropCollection() methods.</summary>
    internal class __Drop: IMongoDriverDatabaseMethod, IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "drop", "dropDatabase", "dropCollection" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(string.IsNullOrWhiteSpace(func.Collection))
            {
                if(func.Arguments.Length < 1) { throw new MongoArgumentException("Arguments missing."); }

                if(func.Name.ToLower() == "dropdatabase")
                {
                    func.GetClient().DropDatabase(func.Arguments[0].FromJsString());
                }
                else if(func.Name.ToLower() == "dropcollection")
                {
                    func.GetDatabase().DropCollection(func.Arguments[0].FromJsString());
                }
            }
            else if(func.Name.ToLower() == "drop")
            {
                if(func.Arguments.Length > 0) { throw new MongoArgumentException("Too many arguments."); }

                func.GetDatabase().DropCollection(func.Collection);
            }
            else { throw new MongoException("Invalid method call."); }

            return new MongoDriverResultTable();
        }
    }
}

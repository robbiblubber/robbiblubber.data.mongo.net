﻿using System;

using MongoDB.Driver;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the createCollection() method.</summary>
    internal class __CreateCollection: IMongoDriverDatabaseMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "createCollection" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(func.Arguments.Length < 1) { throw new MongoArgumentException("Argument missing."); }

            func.GetDatabase().CreateCollection(func.Arguments[0].FromJsString(), func.GetOptions<CreateCollectionOptions>());
            return new MongoDriverResultTable();
        }
    }
}

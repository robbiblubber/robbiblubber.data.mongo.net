﻿using System;
using System.Collections.Generic;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;
using MongoDB.Driver;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the delete(), deleteOne(), deleteMany(), remove() methods.</summary>
    internal class __Delete: IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "delete", "deleteOne", "deleteMany", "remove" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            BsonDocument filter;
            
            if(func.Arguments.Length < 1) { throw new MongoArgumentException("Argument missing."); }
            if(func.Arguments.Length > 2) { throw new MongoArgumentException("Too many arguments."); }
            
            try
            {
                filter = BsonDocument.Parse(func.Arguments[0]);
            }
            catch(Exception ex) { throw new MongoArgumentException("Invalid arguments.", ex); }

            bool onlyOne = func.Name.ToLower() == "deleteone";
            if(func.Name.ToLower() == "remove")
            {
                string remarg = "false";
                if(func.Arguments.Length > 1) { remarg = func.Arguments[1].FromJsString().ToLower(); }
                if((remarg == "true") || (remarg == "1")) { onlyOne = true; }
            }

            DeleteResult rval;
            try
            {
                if(onlyOne)
                {
                    rval = func.GetCollection().DeleteOne(filter, func.GetOptions<DeleteOptions>());
                }
                else
                {
                    rval = func.GetCollection().DeleteMany(filter, func.GetOptions<DeleteOptions>());
                }
            }
            catch(Exception ex) { throw new MongoExecutionException(ex.Message, ex); }

            return rval.ToBsonDocument().ToBsonTable(Convert.ToInt32(rval.DeletedCount));
        }
    }
}

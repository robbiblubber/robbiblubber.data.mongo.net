﻿using System;
using System.Collections.Generic;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;
using MongoDB.Driver;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the dropIndex(), dropAllIndexes() methods.</summary>
    internal class __DropIndex: IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "dropIndex", "dropAllIndexes" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(func.Name.ToLower() == "dropallindexes")
            {
                if(func.Arguments.Length > 0) { throw new MongoArgumentException("Too many arguments."); }

                func.GetCollection().Indexes.DropAll();
            }
            else
            {
                if(func.Arguments.Length < 1) { throw new MongoArgumentException("Argument missing."); }
                if(func.Arguments.Length > 1) { throw new MongoArgumentException("Too many arguments."); }

                func.GetCollection().Indexes.DropOne(func.Arguments[0]);
            }

            return new MongoDriverResultTable();
        }
    }
}

﻿using System;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Driver;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the renameCollection() method.</summary>
    internal class __RenameCollection: IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "renameCollection" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(func.Arguments.Length < 1) { throw new MongoArgumentException("Missing argument."); }
            if(func.Arguments.Length > 2) { throw new MongoArgumentException("Too many arguments."); }

            try
            {
                func.GetDatabase().RenameCollection(func.Collection, func.Arguments[0].FromJsString(), func.GetOptions<RenameCollectionOptions>());
            }
            catch(Exception ex) { throw new MongoExecutionException(ex.Message, ex); }

            return new MongoDriverResultTable();
        }
    }
}

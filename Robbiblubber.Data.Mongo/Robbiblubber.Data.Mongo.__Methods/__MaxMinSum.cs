﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the max(), min(), sum(), avg() methods.</summary>
    internal class __MaxMinSum: IMongoDriverCursorMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "max", "min", "sum", "avg" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(func.Input.Columns.Count == 0) return func.Input;

            MongoDriverResultTable rval = func.Input;
            if((func.Arguments.Length > 0) || (rval.Columns[0].DataType == typeof(BsonDocument))) { rval = __Select.__Execute(func); }

            List<Tuple<string, object>> vals = new List<Tuple<string, object>>();
            object ag;

            foreach(DataColumn j in rval.Columns)
            {
                if((func.Name.ToLower() == "sum") || (func.Name.ToLower() == "avg"))
                {
                    ag = (double) 0;
                }
                else if(func.Name.ToLower() == "max")
                {
                    ag = null;
                }
                else { ag = rval.Rows[0][j.ColumnName]; }

                foreach(DataRow k in rval.Rows)
                {
                    ag = func.Name.ToLower().Aggregate(ag, k[j.ColumnName]);
                }
                
                if(func.Name.ToLower() == "avg")
                {
                    vals.Add(new Tuple<string, object>(j.ColumnName, (((double) ag) / Convert.ToDouble(rval.Rows.Count))));
                }
                else { vals.Add(new Tuple<string, object>(j.ColumnName, ag)); }
            }

            rval = new MongoDriverResultTable(rval.RecordsAffected);
            foreach(Tuple<string, object> k in vals)
            {
                rval.Columns.Add(k.Item1, (k.Item2 == null) ? typeof(object) : k.Item2.GetType());
            }

            DataRow row = rval.NewRow();
            foreach(Tuple<string, object> k in vals)
            {
                row[k.Item1] = k.Item2;
            }
            rval.Rows.Add(row);

            return rval;
        }
    }
}

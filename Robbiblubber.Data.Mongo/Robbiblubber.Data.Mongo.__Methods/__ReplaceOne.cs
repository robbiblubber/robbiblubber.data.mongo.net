﻿using System;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Driver;
using MongoDB.Bson;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the replaceOne() method.</summary>
    internal class __ReplaceOne: IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "replaceOne" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(func.Arguments.Length < 2) { throw new MongoArgumentException("Missing argument."); }
            if(func.Arguments.Length > 3) { throw new MongoArgumentException("Too many arguments."); }

            BsonDocument filter;
            BsonDocument doc;
            MongoDriverResultTable rval = new MongoDriverResultTable();

            try
            {
                filter = BsonDocument.Parse(func.Arguments[0]);
                doc = BsonDocument.Parse(func.Arguments[1]);
            }
            catch(Exception ex) { throw new MongoArgumentException("Invalid arguments.", ex); }

            ReplaceOneResult result = func.GetCollection().ReplaceOne(filter, doc,  func.GetOptions<ReplaceOptions>());
            return result.ToBsonDocument().ToBsonTable(Convert.ToInt32(result.ModifiedCount));
        }
    }
}

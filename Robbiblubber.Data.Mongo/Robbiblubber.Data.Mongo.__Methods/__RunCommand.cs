﻿using System;

using MongoDB.Bson;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the runCommand() method.</summary>
    internal class __RunCommand: IMongoDriverDatabaseMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "runCommand" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(func.Arguments.Length < 1) { throw new MongoArgumentException("Arguments missing."); }

            return func.GetDatabase().RunCommand<BsonDocument>(BsonDocument.Parse(func.Arguments[0].Trim())).ToBsonTable();
        }
    }
}

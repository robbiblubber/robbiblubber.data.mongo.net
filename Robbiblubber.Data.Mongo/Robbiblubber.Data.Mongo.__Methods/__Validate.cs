﻿using System;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Driver;
using MongoDB.Bson;

namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the validate() method.</summary>
    internal class __Validate: IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "validate" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(func.Arguments.Length > 1) { throw new MongoArgumentException("Too many arguments."); }

            string full = "false";
            if(func.Arguments.Length > 0) { full = func.Arguments[0].FromJsString().ToLower(); }
            if(full == "1") { full = "true"; }

            BsonDocument result = func.GetDatabase().RunCommand<BsonDocument>(BsonDocument.Parse("{ validate: \"" + func.Collection + "\", full: " + full + " }"));
            return result.ToBsonTable();
        }
    }
}

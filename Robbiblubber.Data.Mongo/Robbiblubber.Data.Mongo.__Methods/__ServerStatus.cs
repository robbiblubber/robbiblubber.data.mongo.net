﻿using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the getProfilingLevel(), serverStatus(), stats()  methods.</summary>
    public class __ServerStatus: IMongoDriverDatabaseMethod, IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods 
        { 
            get 
            {
                return new string[] { "stats", "serverStatus", "getProfilingLevel" };
            }
        }
        

        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(string.IsNullOrWhiteSpace(func.Collection))
            {
                if(func.Name.ToLower() == "getprofilinglevel")
                {
                    if(func.Arguments.Length > 0) { throw new MongoArgumentException("Too many arguments."); }

                    return func.GetDatabase().RunCommand<BsonDocument>(BsonDocument.Parse("{ profile: 1 }")).ToBsonTable();
                }

                if(func.Name.ToLower() == "serverstatus")
                {
                    if(func.Arguments.Length > 0) { throw new MongoArgumentException("Too many arguments."); }

                    return func.GetDatabase().RunCommand<BsonDocument>(BsonDocument.Parse("{ serverStatus: 1 }")).ToBsonTable();
                }
            }

            if(func.Name.ToLower() == "stats")
            {
                if(func.Arguments.Length > 1) { throw new MongoArgumentException("Too many arguments."); }

                string scale = ((func.Arguments.Length == 0) ? "1" : func.Arguments[0]);
                if(string.IsNullOrWhiteSpace(func.Collection))
                {
                    return func.GetDatabase().RunCommand<BsonDocument>(BsonDocument.Parse("{ dbStats: 1, scale: " + scale + " }")).ToBsonTable();
                }
                else
                {
                    return func.GetDatabase().RunCommand<BsonDocument>(BsonDocument.Parse("{ collStats: \"" + func.Collection + "\", scale: " + scale + " }")).ToBsonTable();
                }
            }

            throw new MongoUnknownFunctionException("Unknown function.");
        }
    }
}

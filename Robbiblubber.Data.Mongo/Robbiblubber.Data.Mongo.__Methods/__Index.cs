﻿using System;
using System.Collections.Generic;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;
using MongoDB.Driver;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the index(), createIndex() methods.</summary>
    internal class __Index: IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "index", "createIndex" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            List<string> args = new List<string>(func.Arguments);

            if(func.Name.ToLower() == "index")
            {
                string opt = "{";
                for(int i = 1; i < args.Count; i++)
                {
                    if(i > 1) { opt += ","; }
                    opt += (args[i] + ": 1");
                }
                args[1] = (opt + "}");

                while(args.Count > 2) { args.RemoveAt(2); }
            }

            __FunctionCall ops = new __FunctionCall(func.Name, args);
            try
            {
                int bse = 0;
                if(!args[0].Trim().StartsWith("{")) { bse = 1; }
                return func.GetCollection().Indexes.CreateOne(new CreateIndexModel<BsonDocument>(BsonDocument.Parse(args[bse]), ops.GetOptions<CreateIndexOptions>(bse)), ops.GetOptions<CreateOneIndexOptions>(bse)).ToTable();
            }
            catch(Exception ex) { throw new MongoExecutionException(ex.Message, ex); }
        }
    }
}

﻿using System;
using System.Collections.Generic;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;
using MongoDB.Driver;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the distinct() method.</summary>
    internal class __Distinct: IMongoDriverCollectionMethod, IMongoDriverCursorMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "distinct" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            BsonDocument filter;

            if(string.IsNullOrWhiteSpace(func.Collection))
            {
                MongoDriverResultTable rval = func.Input;
                if(func.Arguments.Length > 0) { rval = __Select.__Execute(func); }

                return rval.MakeDistinct();
            }

            try
            {
                if(func.Arguments.Length < 1) { throw new MongoArgumentException("Argument missing."); }

                if(func.Arguments.Length < 2)
                {
                    filter = BsonDocument.Parse("{}");
                }
                else { filter = BsonDocument.Parse(func.Arguments[1]); }
            }
            catch(Exception ex) { throw new MongoArgumentException("Invalid arguments.", ex); }

            return func.GetCollection().Distinct<object>(func.Arguments[0].FromJsString(), filter, func.GetOptions<DistinctOptions>()).ToEnumerable().ToTable();
        }
    }
}

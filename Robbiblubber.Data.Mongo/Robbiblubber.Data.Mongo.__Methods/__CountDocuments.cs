﻿using System;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;
using MongoDB.Driver;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the countDocuments(), estimatedDocumentCount() methods.</summary>
    internal class __CountDocuments: IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "countDocuments", "estimatedDocumentCount" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(func.Name.ToLower() == "countdocuments")
            {
                BsonDocument filter;
                if(func.Arguments.Length > 2) { throw new MongoArgumentException("Too many arguments."); }

                try
                {
                    if((func.Arguments.Length == 0) || (func.Name.ToLower() == "select"))
                    {
                        filter = BsonDocument.Parse("{}");
                    }
                    else { filter = BsonDocument.Parse(func.Arguments[0]); }
                }
                catch(Exception ex) { throw new MongoArgumentException("Invalid arguments.", ex); }

                try
                {
                    return new MongoDriverResultTable("countDocuments", func.GetCollection().CountDocuments(filter, func.GetOptions<CountOptions>()));
                }
                catch(Exception ex) { throw new MongoExecutionException(ex.Message, ex); }
            }
            else
            {
                if(func.Arguments.Length > 1) { throw new MongoArgumentException("Too many arguments."); }

                try
                {
                    return new MongoDriverResultTable("estimatedDocumentCount", func.GetCollection().EstimatedDocumentCount(func.GetOptions<EstimatedDocumentCountOptions>()));
                }
                catch(Exception ex) { throw new MongoExecutionException(ex.Message, ex); }
            }
        }
    }
}

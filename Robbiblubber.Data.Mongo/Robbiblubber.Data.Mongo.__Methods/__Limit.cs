﻿using System;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the limit(), first() methods.</summary>
    internal class __Limit: IMongoDriverCursorMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "limit", "first", "last" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(func.Arguments.Length > 1) { throw new MongoArgumentException("Too many arguments."); }

            int l = 1;
            bool last = (func.Name.ToLower() == "last");
            if(func.Arguments.Length > 0)
            {
                l = Convert.ToInt32(func.Arguments[0]);
            }
            else if(func.Name.ToLower() == "limit") { throw new MongoArgumentException("Missing arguments."); }


            while(func.Input.Rows.Count > l) { func.Input.Rows.RemoveAt(last ? 0 : l); }

            return func.Input;
        }
    }
}

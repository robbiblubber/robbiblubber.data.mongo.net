﻿using System;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;
using MongoDB.Driver;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the find(), findOne(), select() methods on collections.</summary>
    internal class __Find: IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "find", "findOne", "select" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            BsonDocument filter;

            if(func.Name.ToLower() == "select") 
            { 
                ((__FunctionCall) func)._Statement.Functions.Insert(1, new __Func("select", func.Arguments));
            }

            try
            {
                if((func.Arguments.Length == 0) || (func.Name.ToLower() == "select"))
                {
                    filter = BsonDocument.Parse("{}");
                }
                else { filter = BsonDocument.Parse(func.Arguments[0]); }
            }
            catch(Exception ex) { throw new MongoArgumentException("Invalid arguments.", ex); }

            MongoDriverResultTable rval = func.GetCollection().Find(filter, func.GetOptions<FindOptions>()).ToEnumerable().ToBsonTable();

            if(func.Name.ToLower() == "findone")
            {
                while(rval.Rows.Count > 1)
                {
                    rval.Rows[rval.Rows.Count - 1].Delete();
                }
            }

            return rval;
        }
    }
}

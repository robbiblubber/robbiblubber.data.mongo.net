﻿using System;

using MongoDB.Bson;
using MongoDB.Driver;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the createView() method.</summary>
    internal class __CreateView: IMongoDriverDatabaseMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "createView" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(func.Arguments.Length < 3) { throw new MongoArgumentException("Argument missing."); }

            try
            {
                func.GetDatabase().CreateView<BsonDocument, BsonDocument>(func.Arguments[0].FromJsString(),
                                                                          func.Arguments[1].FromJsString(),
                                                                          func.Arguments[2].ParseArray(),
                                                                          func.GetOptions<CreateViewOptions<BsonDocument>>());
            }
            catch(Exception ex) { throw new MongoExecutionException(ex.Message, ex); }

            return new MongoDriverResultTable();
        }
    }
}

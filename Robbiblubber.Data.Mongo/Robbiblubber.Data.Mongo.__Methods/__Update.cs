﻿using System;
using System.Collections.Generic;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;
using MongoDB.Driver;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the update(), updateOne(), updateMany() method.</summary>
    internal class __Update: IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "update", "updateOne", "updateMany" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            BsonDocument filter, update;
            if(func.Arguments.Length < 2) { throw new MongoArgumentException("Argument missing."); }
            if(func.Arguments.Length > 3) { throw new MongoArgumentException("Too many arguments."); }

            try
            {
                filter = BsonDocument.Parse(func.Arguments[0]);
                update = BsonDocument.Parse(func.Arguments[1]);
            }
            catch(Exception ex) { throw new MongoArgumentException("Invalid arguments.", ex); }

            UpdateResult rval;
            try
            {
                if(func.Name.ToLower() == "updateone")
                {
                    rval = func.GetCollection().UpdateOne(filter, update, func.GetOptions<UpdateOptions>());
                }
                else
                {
                    rval = func.GetCollection().UpdateMany(filter, update, func.GetOptions<UpdateOptions>());
                }
            }
            catch(Exception ex) { throw new MongoExecutionException(ex.Message, ex); }

            return rval.ToBsonDocument().ToBsonTable(Convert.ToInt32(rval.ModifiedCount));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the sort(), orderBy(), asc(), desc() methods.</summary>
    internal class __Sort: IMongoDriverCursorMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "sort", "orderBy", "asc", "desc" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            List<Tuple<string, bool>> fields = new List<Tuple<string, bool>>();

            if(func.Name.ToLower() == "orderby")
            {
                foreach(string k in func.Arguments)
                {
                    string s = k;
                    bool asc = true;
                    if(s.ToLower().EndsWith(" asc"))
                    {
                        s = s.Substring(0, s.Length - 4);
                    }
                    else if(s.ToLower().EndsWith(" desc"))
                    {
                        asc = false;
                        s = s.Substring(0, s.Length - 5);
                    }

                    s = func.Input.GetColKey(s.FromJsString());
                    if(!string.IsNullOrWhiteSpace(s)) { fields.Add(new Tuple<string, bool>(s, asc)); }
                }
            }
            else if(func.Name.ToLower() == "sort")
            {
                if(func.Arguments.Length < 1) { throw new MongoArgumentException("Missing argument."); }

                foreach(BsonElement k in BsonDocument.Parse(func.Arguments[0]))
                {
                    fields.Add(new Tuple<string, bool>(k.Name, k.Value.AsInt32 > 0));
                }
            }
            else if((func.Name.ToLower() == "asc") || (func.Name.ToLower() == "desc"))
            {
                bool asc = (func.Name.ToLower() == "asc");

                foreach(string k in func.Arguments)
                {
                    string s = func.Input.GetColKey(k.FromJsString());
                    if(!string.IsNullOrWhiteSpace(s)) { fields.Add(new Tuple<string, bool>(s, asc)); }
                }

                if((func.Arguments.Length == 0) && (func.Input.Columns.Count > 0))
                {
                    fields.Add(new Tuple<string, bool>(func.Input.Columns[0].ColumnName, asc));
                }
            }

            if(fields.Count == 0) { return func.Input; }

            List<Tuple<DataRow, int>> sorted = new List<Tuple<DataRow, int>>();
            for(int i = 0; i < func.Input.Rows.Count; i++)
            {
                sorted.Add(new Tuple<DataRow, int>(func.Input.Rows[i], i));
            }

            sorted.Sort(new __RowComparer(fields));

            MongoDriverResultTable rval = new MongoDriverResultTable();
            foreach(DataColumn i in func.Input.Columns) { rval.Columns.Add(i.ColumnName, i.DataType); }
            foreach(Tuple<DataRow, int> i in sorted)
            {
                DataRow r = rval.NewRow();
                foreach(DataColumn k in rval.Columns) { r[k.ColumnName] = i.Item1[k.ColumnName]; }
                rval.Rows.Add(r);
            }

            return rval;
        }
    }
}

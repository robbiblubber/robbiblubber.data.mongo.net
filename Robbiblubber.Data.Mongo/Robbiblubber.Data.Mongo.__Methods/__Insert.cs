﻿using System;
using System.Collections.Generic;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Bson;
using MongoDB.Driver;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the insert(), insertOne(), insertMany() methods.</summary>
    internal class __Insert: IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "insert", "insertOne", "insertMany" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            if(func.Arguments.Length < 1) { throw new MongoArgumentException("Argument missing."); }
            if(func.Arguments.Length > 2) { throw new MongoArgumentException("Too many arguments."); }

            if(func.Name.ToLower() == "insertone")
            {
                BsonDocument doc;

                try
                {
                    doc = BsonDocument.Parse(func.Arguments[0]);
                }
                catch(Exception ex) { throw new MongoArgumentException("Invalid arguments.", ex); }

                try
                {
                    func.GetCollection().InsertOne(doc, func.GetOptions<InsertOneOptions>());
                }
                catch(Exception ex) { throw new MongoExecutionException(ex.Message, ex); }

                return new MongoDriverResultTable(1);
            }

            if(func.Name.ToLower() == "insertmany")
            {
                List<BsonDocument> docs;

                try
                {
                    docs = func.Arguments[0].ParseArray();
                }
                catch(Exception ex) { throw new MongoArgumentException("Invalid arguments.", ex); }

                try
                {
                    func.GetCollection().InsertMany(docs, func.GetOptions<InsertManyOptions>());
                }
                catch(Exception ex) { throw new MongoExecutionException(ex.Message, ex); }

                return new MongoDriverResultTable(docs.Count);
            }

            if(func.Name.ToLower() == "insert")
            {
                List<BsonDocument> docs = new List<BsonDocument>();

                try
                {
                    foreach(string i in func.Arguments) { docs.Add(BsonDocument.Parse(i)); }
                }
                catch(Exception ex) { throw new MongoArgumentException("Invalid arguments.", ex); }

                try
                {
                    func.GetCollection().InsertMany(docs);
                }
                catch(Exception ex) { throw new MongoExecutionException(ex.Message, ex); }

                return new MongoDriverResultTable(docs.Count);
            }

            throw new MongoUnknownFunctionException("Unknown function.");
        }
    }
}

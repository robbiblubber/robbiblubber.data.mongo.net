﻿using System;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Exceptions;
using Robbiblubber.Data.Mongo.Extensibility;

using MongoDB.Driver;
using MongoDB.Bson;



namespace Robbiblubber.Data.Mongo.__Methods
{
    /// <summary>This class implements the hideIndex(), unhideIndex method().</summary>
    internal class __HideIndex: IMongoDriverCollectionMethod
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMongoDriverDatabaseMethod                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the names of the methods implemented in this class.</summary>
        public string[] Methods
        {
            get
            {
                return new string[] { "hideIndex", "unhideIndex" };
            }
        }


        /// <summary>Executes the method.</summary>
        /// <param name="func">Function call data.</param>
        /// <returns>Returns a result table.</returns>
        public MongoDriverResultTable Execute(IMongoDriverFunctionCall func)
        {
            string hidden = ((func.Name.ToLower() == "hideindex") ? "true" : "false");

            BsonDocument result = func.GetDatabase().RunCommand<BsonDocument>(BsonDocument.Parse("{ collMod: \"" +  func.Collection + "\"," +
                                                                                           "index: { name: \"" + func.Arguments[0].FromJsString() + "\", hidden: " + hidden + ", " + " } }"));
            if(result["ok"].AsInt32 == 0) { throw new MongoExecutionException(result["errmsg"].AsString); }
            return result.ToBsonTable();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using MongoDB.Driver;

using Robbiblubber.Data.Mongo.__Basic;
using Robbiblubber.Data.Mongo.Extensibility;
using Robbiblubber.Util;
using Robbiblubber.Util.Collections;



namespace Robbiblubber.Data.Mongo
{
    /// <summary>This class provides a MongoDB connection.</summary>
    public sealed class MongoConnection: IDbConnection
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Parser.</summary>
        internal __Parser _Parser;

        /// <summary>MongoDB client.</summary>
        internal IMongoClient _Client;

        /// <summary>Database object.</summary>
        internal IMongoDatabase _Database;

        /// <summary>Implemented methods.</summary>
        private TList<IMongoDriverExecutable> _Methods;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public MongoConnection()
        {
            _Parser = new __Parser();

            _Methods = new TList<IMongoDriverExecutable>();
            foreach(IMongoDriverMethod i in ClassOp.LoadSubtypesOf<IMongoDriverMethod>(ClassSearchOptions.LOADED_ASSEMBLIES))
            {
                _Methods.Add(i);
            }
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="connectionString">Connection string.</param>
        public MongoConnection(string connectionString): this()
        {
            ConnectionString = connectionString;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the underlying MongoDB client object.</summary>
        public object Client
        {
            get { return _Client;  }
        }


        /// <summary>Gets or sets the underlying MongoDB database object.</summary>
        public object Database
        {
            get { return _Database; }
            set { _Database = (IMongoDatabase) value; }
        }


        /// <summary>Gets a list of driver extension implementations for this connection.</summary>
        public IList<IMongoDriverExecutable> Extensions
        {
            get;
        } = new TList<IMongoDriverExecutable>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an extension implementation to the connection.</summary>
        /// <param name="implementation">Extension implementation.</param>
        public void AddImplementation(IMongoDriverImplementation implementation)
        {
            foreach(IMongoDriverExecutable i in implementation.GetExecutables()) { Extensions.Add(i); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private properties                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns all applicable database methods.</summary>
        internal IEnumerable<IMongoDriverDatabaseMethod> _DatabaseMethods
        {
            get
            {
                return _Methods.Where(m => m is IMongoDriverDatabaseMethod).Union(Extensions.Where(m => m is IMongoDriverDatabaseMethod)).Cast<IMongoDriverDatabaseMethod>();
            }
        }


        /// <summary>Returns all applicable collection methods.</summary>
        internal IEnumerable<IMongoDriverCollectionMethod> _CollectionMethods
        {
            get
            {
                return _Methods.Where(m => m is IMongoDriverCollectionMethod).Union(Extensions.Where(m => m is IMongoDriverCollectionMethod)).Cast<IMongoDriverCollectionMethod>();
            }
        }


        /// <summary>Returns all applicable cursor methods.</summary>
        internal IEnumerable<IMongoDriverCursorMethod> _CursorMethods
        {
            get
            {
                return _Methods.Where(m => m is IMongoDriverCursorMethod).Union(Extensions.Where(m => m is IMongoDriverCursorMethod)).Cast<IMongoDriverCursorMethod>();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a database command.</summary>
        /// <returns>Command object.</returns>
        public MongoCommand CreateCommand()
        {
            return new MongoCommand(this);
        }


        /// <summary>Creates a database command.</summary>
        /// <param name="commandText">Command text.</param>
        /// <returns>Command object.</returns>
        public MongoCommand CreateCommand(string commandText)
        {
            return new MongoCommand(this, commandText);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbConnection                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the connection string.</summary>
        public string ConnectionString
        {
            get;  set;
        }


        /// <summary>Gets the connection timeout.</summary>
        public int ConnectionTimeout
        {
            get { return 0; }
        }


        /// <summary>Gets the database name.</summary>
        string IDbConnection.Database
        {
            get 
            {
                if(Database == null) return null;
                return _Database.DatabaseNamespace.DatabaseName; 
            }
        }


        /// <summary>Gets the database state.</summary>
        public ConnectionState State
        {
            get
            {
                if(Client == null) return ConnectionState.Closed;
                return ConnectionState.Open;
            }
        }


        /// <summary>Begins a transaction.</summary>
        /// <returns>Transaction.</returns>
        public IDbTransaction BeginTransaction()
        {
            return BeginTransaction(IsolationLevel.Unspecified);
        }


        /// <summary>Begins a transaction.</summary>
        /// <param name="il">Isolation level.</param>
        /// <returns>Transaction.</returns>
        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            throw new NotSupportedException("Driver does not support transactions.");
        }


        /// <summary>Changes the database.</summary>
        /// <param name="databaseName"></param>
        public void ChangeDatabase(string databaseName)
        {
            Database = _Client.GetDatabase(databaseName);
        }


        /// <summary>Closes the connection.</summary>
        public void Close()
        {
            _Client = null;
        }


        /// <summary>Creates a database command.</summary>
        /// <returns>Command object.</returns>
        IDbCommand IDbConnection.CreateCommand()
        {
            return new MongoCommand(this);
        }


        /// <summary>Disposes the object.</summary>
        public void Dispose()
        {
            Close();
        }


        /// <summary>Opens the connection</summary>
        public void Open()
        {
            string conn = "";
            string db = "";
            if(ConnectionString.StartsWith("mongodb+"))
            {
                conn = ConnectionString;
                db = conn.Substring(14).After("/").Before("?");
            }
            else
            {
                Ddp cs = Ddp.Create(ConnectionString);
                conn = "mongodb+srv://" + cs.Get<string>("user", cs.Get<string>("uid", cs.Get<string>("username"))) + ":" +
                                          cs.Get("password", cs.Get<string>("passwd", cs.Get<string>("pwd"))) + "@" +
                                          cs.Get("cluster", cs.Get<string>("server")) + "/" +
                                          (db = cs.Get<string>("database", cs.Get<string>("db", "test"))) +
                                          "?retryWrites=" + (cs.Get<bool>("retryWrites", true) ? "true" : "false") +
                                          "&w=" + (cs.Get<string>("w", cs.Get<string>("writeConcern", "majority")));
            }

            _Client = new MongoClient(conn);
            _Database = _Client.GetDatabase(db);
        }
    }
}

﻿using System;
using System.Collections;
using System.Data;

using Robbiblubber.Util.Collections;



namespace Robbiblubber.Data.Mongo
{
    /// <summary>This class implements a MongoDB parameter collection.</summary>
    public sealed class MongoParameterCollection: TList<MongoParameter>, IDataParameterCollection, IList, ICollection, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MongoParameterCollection(): base()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets a parameter by its name.</summary>
        /// <param name="parameterName">Parameter name.</param>
        public MongoParameter this[string parameterName]
        {
            get 
            {
                foreach(MongoParameter i in this)
                {
                    if(i.ParameterName == parameterName) { return i; }
                }

                throw new IndexOutOfRangeException();
            }
            set { this[IndexOf(parameterName)] = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a parameter to the collection.</summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <param name="value">Parameter value.</param>
        public void Add(string parameterName, object value)
        {
            Add(new MongoParameter(parameterName, value));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] List<MongoParameter>                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Adds a parameter to the collection.</summary>
        /// <param name="item">Parameter item.</param>
        public new void Add(MongoParameter item)
        {
            if(Contains(item.ParameterName)) { throw new ArgumentException("Duplicate paramter name."); }

            base.Add(item);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDataParameterCollection                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets a parameter by its name.</summary>
        /// <param name="parameterName">Parameter name.</param>
        object IDataParameterCollection.this[string parameterName]
        {
            get { return this[parameterName]; }
            set { this[parameterName] = (MongoParameter) value; }
        }


        /// <summary>Returns if the collection contains a parameter.</summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <returns>Returns TRUE if the collection contains a parameter with the given name, otherwise returns FALSE.</returns>
        public bool Contains(string parameterName)
        {
            foreach(MongoParameter i in this)
            {
                if(i.ParameterName == parameterName) { return true; }
            }

            return false;
        }


        /// <summary>Gets the index of a parameter by its name.</summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <returns>Index.</returns>
        public int IndexOf(string parameterName)
        {
            for(int i = 0; i < Count; i++)
            {
                if(this[i].ParameterName == parameterName) { return i; }
            }

            throw new IndexOutOfRangeException();
        }


        /// <summary>Removes a parameter by its name.</summary>
        /// <param name="parameterName">Parameter name.</param>
        public void RemoveAt(string parameterName)
        {
            RemoveAt(IndexOf(parameterName));
        }

    }
}

﻿using System;
using System.Data;

using Robbiblubber.Data.Mongo.Extensibility;
using Robbiblubber.Data.Mongo.__Basic;



namespace Robbiblubber.Data.Mongo
{
    /// <summary>This class provides a MongoDB data reader.</summary>
    public sealed class MongoDataReader: IDataReader
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Table.</summary>
        private MongoDriverResultTable _Table;

        /// <summary>Current row index.</summary>
        private int _Pos = -1;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="table">Data table.</param>
        internal MongoDataReader(MongoDriverResultTable table, int depth = 0)
        {
            _Table = table;
            Depth = depth;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDataReader                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a column value by its index.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Column value.</returns>
        public object this[int i]
        {
            get { return _Table.Rows[_Pos][i]; }
        }


        /// <summary>Gets a column value by its name.</summary>
        /// <param name="name">Column name.</param>
        /// <returns>Column value.</returns>
        public object this[string name]
        {
            get { return _Table.Rows[_Pos][name]; }
        }


        /// <summary>Gets a value indicating the depth of nesting for the current row.</summary>
        public int Depth
        {
            get; private set;
        }


        /// <summary>Gets if the reader is closed.</summary>
        public bool IsClosed
        {
            get; private set;
        } = false;


        /// <summary>Gets the number of records inserted, changed, or deleted by the statement.</summary>
        public int RecordsAffected
        {
            get { return _Table.RecordsAffected; }
        }


        /// <summary>Gets the number of fields in this reader.</summary>
        public int FieldCount
        {
            get { return _Table.Columns.Count; }
        }


        /// <summary>Closes the reader.</summary>
        public void Close()
        {
            IsClosed = true;
        }

        public void Dispose()
        {
            _Table = null;
        }


        /// <summary>Gets the field value as a Boolean.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Field value.</returns>
        public bool GetBoolean(int i)
        {
            return (bool) _Table.Rows[_Pos][i];
        }


        /// <summary>Gets the field value as a byte.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Field value.</returns>
        public byte GetByte(int i)
        {
            return (byte) _Table.Rows[_Pos][i];
        }


        /// <summary>Reads a stream of bytes from the current field into a buffer.</summary>
        /// <param name="i">Index.</param>
        /// <param name="fieldoffset">Field offset.</param>
        /// <param name="buffer">Buffer.</param>
        /// <param name="bufferoffset">Buffer offset.</param>
        /// <param name="length">Length.</param>
        /// <returns>Returns the number of bytes copied.</returns>
        public long GetBytes(int i, long fieldoffset, byte[] buffer, int bufferoffset, int length)
        {
            byte[] v = ((byte[]) _Table.Rows[_Pos][i]);
            long l = Convert.ToInt64(v.Length) - fieldoffset;
            if(l > length) { l = length; }
            
            for(int k = 0; k < l; k++)
            {
                buffer[k + bufferoffset] = v[k + fieldoffset];
            }

            return l;
        }


        /// <summary>Gets the field value as a character.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Field value.</returns>
        public char GetChar(int i)
        {
            return (char) _Table.Rows[_Pos][i];
        }


        /// <summary>Reads a stream of characters from the current field into a buffer.</summary>
        /// <param name="i">Index.</param>
        /// <param name="fieldOffset">Field offset.</param>
        /// <param name="buffer">Buffer.</param>
        /// <param name="bufferoffset">Buffer offset.</param>
        /// <param name="length">Length.</param>
        /// <returns>Returns the number of bytes copied.</returns>
        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            char[] v = ((char[]) _Table.Rows[_Pos][i]);
            long l = Convert.ToInt64(v.Length) - fieldoffset;
            if(l > length) { l = length; }

            for(int k = 0; k < l; k++)
            {
                buffer[k + bufferoffset] = v[k + fieldoffset];
            }

            return l;
        }


        /// <summary>Gets a data reader for a field.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Data reader.</returns>
        public IDataReader GetData(int i)
        {
            return new MongoDataReader(GetValue(i).ParseTable(), Depth + 1);
        }


        /// <summary>Gets the data typ name for a field.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Data type name.</returns>
        public string GetDataTypeName(int i)
        {
            return GetFieldType(i).Name;
        }


        /// <summary>Gets the field value as a date/time.</summary>
        /// <param name="i">Index.</param>
        /// <returns>DateTime.</returns>
        public DateTime GetDateTime(int i)
        {
            return (DateTime) _Table.Rows[_Pos][i];
        }


        /// <summary>Gets the field value as a decimal.</summary>
        /// <param name="i">Index.</param>
        /// <returns>DateTime.</returns>
        public decimal GetDecimal(int i)
        {
            return (decimal) _Table.Rows[_Pos][i];
        }


        /// <summary>Gets the field value as a double.</summary>
        /// <param name="i">Index.</param>
        /// <returns>DateTime.</returns>
        public double GetDouble(int i)
        {
            return (double) _Table.Rows[_Pos][i];
        }


        /// <summary>Gets the field type for a column.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Field type.</returns>
        public Type GetFieldType(int i)
        {
            return _Table.Columns[i].DataType;
        }


        /// <summary>Gets the field value as a float.</summary>
        /// <param name="i">Index.</param>
        /// <returns>DateTime.</returns>
        public float GetFloat(int i)
        {
            return (float) _Table.Rows[_Pos][i];
        }


        /// <summary>Gets the field value as a GUID.</summary>
        /// <param name="i">Index.</param>
        /// <returns>DateTime.</returns>
        public Guid GetGuid(int i)
        {
            return (Guid) _Table.Rows[_Pos][i];
        }


        /// <summary>Gets the field value as a 16 bit integer.</summary>
        /// <param name="i">Index.</param>
        /// <returns>DateTime.</returns>
        public short GetInt16(int i)
        {
            return (short) _Table.Rows[_Pos][i];
        }


        /// <summary>Gets the field value as a 32 bit integer.</summary>
        /// <param name="i">Index.</param>
        /// <returns>DateTime.</returns>
        public int GetInt32(int i)
        {
            return (int) _Table.Rows[_Pos][i];
        }


        /// <summary>Gets the field value as a 64 bit integer.</summary>
        /// <param name="i">Index.</param>
        /// <returns>DateTime.</returns>
        public long GetInt64(int i)
        {
            return (long) _Table.Rows[_Pos][i];
        }


        /// <summary>Gets the name of a column.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Column name.</returns>
        public string GetName(int i)
        {
            return _Table.Columns[i].ColumnName;
        }


        /// <summary>Gets the ordinal for a column.</summary>
        /// <param name="name">Column name.</param>
        /// <returns>Index.</returns>
        public int GetOrdinal(string name)
        {
            return _Table.Columns[name].Ordinal;
        }


        /// <summary>Gets a schema tabble for the reader.</summary>
        /// <returns>Schema table.</returns>
        public DataTable GetSchemaTable()
        {
            return new DataTableReader(_Table).GetSchemaTable();
        }


        /// <summary>Gets the field value as a string.</summary>
        /// <param name="i">Index.</param>
        /// <returns>DateTime.</returns>
        public string GetString(int i)
        {
            return (string) _Table.Rows[_Pos][i];
        }


        /// <summary>Gets the field value for an index.</summary>
        /// <param name="i">Index.</param>
        /// <returns>DateTime.</returns>
        public object GetValue(int i)
        {
            return _Table.Rows[_Pos][i];
        }


        /// <summary>Populates an array of objects with the column values of the current record.</summary>
        /// <param name="values">Array.</param>
        /// <returns>The number of values copied.</returns>
        public int GetValues(object[] values)
        {
            int l = _Table.Columns.Count;
            if(values.Length < l) { l = values.Length; }

            for(int i = 0; i < l; i++) { values[i] = _Table.Rows[_Pos][i]; }

            return l;
        }


        /// <summary>Gets if the value is NULL.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Returns TRUE for NULL values, otherwise returns FALSE.</returns>
        public bool IsDBNull(int i)
        {
            return (_Table.Rows[_Pos][i] == null);
        }


        /// <summary>Advances the reader to the next result.</summary>
        /// <returns>Returns TRUE if there are more rows, otherwise returns FALSE.</returns>
        public bool NextResult()
        {
            return false;
        }


        /// <summary>Advances the reader to the next record.</summary>
        /// <returns>Returns TRUE if there are more rows, otherwise returns FALSE.</returns>
        public bool Read()
        {
            return (++_Pos < _Table.Rows.Count);
        }
    }
}

﻿using System;
using System.Data;

using Robbiblubber.Data.Mongo.Extensibility;



namespace Robbiblubber.Data.Mongo
{
    /// <summary>This class represents a database command.</summary>
    public class MongoCommand: IDbCommand
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public MongoCommand()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="c">Connection.</param>
        public MongoCommand(MongoConnection c): this()
        {
            Connection = c;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="c">Connection.</param>
        /// <param name="commandText">Command text.</param>
        public MongoCommand(MongoConnection c, string commandText): this(c)
        {
            CommandText = commandText;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the database command.</summary>
        public MongoConnection Connection
        {
            get; set;
        }


        /// <summary>Gets or sets the command paramters.</summary>
        public MongoParameterCollection Parameters
        {
            get;
        } = new MongoParameterCollection();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a data parameter.</summary>
        /// <returns>Data parameter.</returns>
        public MongoParameter CreateParameter()
        {
            return new MongoParameter();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Extecutes the command and returns a data reader containing the command results.</summary>
        /// <returns>Data reader.</returns>
        public MongoDataReader ExecuteReader()
        {
            return ExecuteReader(CommandBehavior.Default);
        }


        /// <summary>Extecutes the command and returns a data reader containing the command results.</summary>
        /// <param name="behavior">Command behavior.</param>
        /// <returns>Data reader.</returns>
        public MongoDataReader ExecuteReader(CommandBehavior behavior)
        {
            return new MongoDataReader(Connection._Parser.Parse(this));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbCommand                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the database command.</summary>
        IDbConnection IDbCommand.Connection
        {
            get { return Connection; }
            set { Connection = (MongoConnection) value; }
        }


        /// <summary>Gets or sets the transaction for this command.</summary>
        public IDbTransaction Transaction
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }


        /// <summary>Gets or sets the command text.</summary>
        public string CommandText
        {
            get; set;
        }


        /// <summary>Gets or sets the timeout for this command.</summary>
        int IDbCommand.CommandTimeout
        {
            get; set;
        }


        /// <summary>Gets or sets the command type for this command.</summary>
        CommandType IDbCommand.CommandType
        {
            get; set;
        } = CommandType.Text;



        /// <summary>Gets or sets the command paramters.</summary>
        IDataParameterCollection IDbCommand.Parameters
        {
            get { return Parameters; }
        }


        /// <summary>Gets or sets the update row source.</summary>
        public UpdateRowSource UpdatedRowSource
        {
            get; set;
        } = UpdateRowSource.Both;


        /// <summary>Cancles the command execution.</summary>
        public void Cancel()
        {}


        /// <summary>Creates a data parameter.</summary>
        /// <returns>Data parameter.</returns>
        IDbDataParameter IDbCommand.CreateParameter()
        {
            return CreateParameter();
        }


        /// <summary>Disposes the command.</summary>
        public void Dispose()
        {}


        /// <summary>Executes a non-query command.</summary>
        /// <returns>Returns the number of rows affected.</returns>
        public int ExecuteNonQuery()
        {
            return Connection._Parser.Parse(this).RecordsAffected;
        }


        /// <summary>Extecutes the command and returns a data reader containing the command results.</summary>
        /// <returns>Data reader.</returns>
        IDataReader IDbCommand.ExecuteReader()
        {
            return ExecuteReader();
        }


        /// <summary>Extecutes the command and returns a data reader containing the command results.</summary>
        /// <param name="behavior">Command behavior.</param>
        /// <returns>Data reader.</returns>
        IDataReader IDbCommand.ExecuteReader(CommandBehavior behavior)
        {
            return ExecuteReader(behavior);
        }


        /// <summary>Executes the command and returns a single value.</summary>
        /// <returns>Value.</returns>
        public object ExecuteScalar()
        {
            MongoDriverResultTable rval = Connection._Parser.Parse(this);

            if(rval.Rows.Count == 0) { return null; }
            return rval.Rows[0][0];
        }


        /// <summary>Prepares the statement.</summary>
        public void Prepare()
        {}
    }
}
